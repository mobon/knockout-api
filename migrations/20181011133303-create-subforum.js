'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    // Create table
    await queryInterface.createTable('Subforums', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING
      },
      icon_id: {
        type: Sequelize.INTEGER
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('NOW()')
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('NOW()')
      }
    });
    // Fill table
    await queryInterface.bulkInsert(
      'Subforums',
      [
        {
          name: 'General Discussion',
          icon_id: 1
        },
        {
          name: 'Game Generals',
          icon_id: 2
        },
        {
          name: 'Gaming',
          icon_id: 3
        },
        {
          name: 'Videos',
          icon_id: 4
        },
        {
          name: 'Politics',
          icon_id: 5
        },
        {
          name: 'News',
          icon_id: 6
        },
        {
          name: 'Film, Television and Music',
          icon_id: 7
        },
        {
          name: 'Hardware & Software',
          icon_id: 7
        },
        {
          name: 'Meta',
          icon_id: 7
        }
      ],
      {}
    );
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Subforums');
  }
};
