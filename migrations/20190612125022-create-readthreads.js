'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('ReadThreads', {
      user_id: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: 'Users',
          key: 'id'
        }
      },
      thread_id: {
        allowNull: false,
        type: Sequelize.INTEGER.UNSIGNED,
        references: {
          model: 'Threads',
          key: 'id'
        }
      },
      last_seen: {
        allowNull: false,
        type: Sequelize.DATE,
        allowNull: false,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.fn('NOW')
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.fn('NOW')
      }
    }).then(() => {
      return queryInterface.sequelize.query(
        'ALTER TABLE `ReadThreads` ADD CONSTRAINT `readthreadkeys` PRIMARY KEY (`thread_id`, `user_id`)'
      ).then(()=> {
        'ALTER TABLE `ReadThreads` ADD CONSTRAINT `Readthreads_ibfk_1` REFERENCES Users (`user_id`)'
      }).then(()=> {
        'ALTER TABLE `Posts` ADD CONSTRAINT `Readthreads_ibfk_2` REFERENCES Posts (`thread_id`)'
      });
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('ReadThreads');
  }
};
