'use strict';

const knex = require('../src/services/knex').knex;

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await knex.schema.alterTable('Users', function(table) {
      table.string('avatar_url').defaultTo('none.webp').alter()
    });
  },

  down: async (queryInterface, Sequelize) => {
    await knex.schema.alterTable('Users', function(table) {
      table.string('avatar_url').defaultTo('').alter()
    });
  }
};
