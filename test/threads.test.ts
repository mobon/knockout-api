import httpStatus from 'http-status';
import jwt from 'jsonwebtoken';
import request from 'supertest';
import { APP_PORT, JWT_SECRET } from '../config/server';
import minAccountAgeSubforums from '../src/constants/minAccountAgeSubforums';
import knex from '../src/services/knex';
import CreateThreadRequest from '../src/v2/schemas/threads/createThreadRequest';
import NewThread from '../src/v2/schemas/threads/newThread';
import redis from '../src/services/redisClient';
import { RAID_MODE_KEY } from '../src/constants/adminSettings';
import UpdateThreadRequest from '../src/v2/schemas/threads/updateThreadRequest';
import { GOLD_MEMBER_GROUPS, MODERATOR_GROUPS } from '../src/constants/userGroups';
import UpdateThreadTagsRequest from '../src/v2/schemas/threads/updateThreadTagsRequest';
import config from '../config/config';

describe('/v2/threads endpoint', () => {
  const { host } = config[process.env.NODE_ENV];
  const TEST_USER_ID = 990;
  const TEST_THREAD_ID = 30000;

  const knockoutJwt = jwt.sign({ id: TEST_USER_ID }, JWT_SECRET, {
    algorithm: 'HS256',
    expiresIn: '2 weeks',
  });

  const api = request(`http://${host}:${APP_PORT}/v2`);

  describe('POST /', () => {
    let createdThreadId = 0;

    beforeAll(async () => {
      await knex('Users').insert({
        id: TEST_USER_ID,
        username: 'Test User',
        avatar_url: 'none.webp',
        usergroup: 1,
        created_at: new Date(2020, 1, 1),
        updated_at: new Date(),
      });
    });

    afterEach(async () => {
      await knex.raw('SET FOREIGN_KEY_CHECKS=0');
      await knex('Threads').where('id', createdThreadId).delete();
      await knex('Posts').where('user_id', TEST_USER_ID).delete();
      await knex.raw('SET FOREIGN_KEY_CHECKS=1');
    });

    afterAll(async () => {
      await knex.raw('SET FOREIGN_KEY_CHECKS=0');
      await knex('Users').where('id', TEST_USER_ID).delete();
      await knex.raw('SET FOREIGN_KEY_CHECKS=1');
    });

    test('creates a thread', async () => {
      const data: CreateThreadRequest = {
        title: 'A Thread',
        content: 'thread post content',
        icon_id: 1,
        subforum_id: 1,
      };

      const expectedResponse: NewThread = {
        id: 1,
        title: 'A Thread',
        iconId: 1,
        userId: TEST_USER_ID,
        subforumId: 1,
        createdAt: null,
        updatedAt: null,
        deletedAt: null,
        locked: false,
        pinned: false,
        backgroundType: null,
        backgroundUrl: null,
      };

      await api
        .post('/threads')
        .send(data)
        .set('Cookie', `knockoutJwt=${knockoutJwt}`)
        .expect((res) => {
          createdThreadId = res.body.id;
          expectedResponse.id = res.body.id;
          expectedResponse.createdAt = res.body.createdAt;
          expectedResponse.updatedAt = res.body.updatedAt;
        })
        .expect(httpStatus.CREATED, expectedResponse);
    });

    test('rejects an invalid user', async () => {
      await knex('Users')
        .update({
          username: null,
        })
        .where('id', TEST_USER_ID);

      const data: CreateThreadRequest = {
        title: 'A Thread',
        content: 'thread post content',
        icon_id: 1,
        subforum_id: 1,
      };

      await api
        .post('/threads')
        .send(data)
        .set('Cookie', `knockoutJwt=${knockoutJwt}`)
        .expect(httpStatus.UNAUTHORIZED);

      await knex('Users')
        .update({
          username: 'Test User',
        })
        .where('id', TEST_USER_ID);
    });

    test('rejects a logged out user', async () => {
      const data: CreateThreadRequest = {
        title: 'A Thread',
        content: 'thread post content',
        icon_id: 1,
        subforum_id: 1,
      };

      await api.post('/threads').send(data).expect(httpStatus.UNAUTHORIZED);
    });

    test('rejects invalid thread content', async () => {
      const data: CreateThreadRequest = {
        title: 'A Thread',
        content: '',
        icon_id: 1,
        subforum_id: 1,
      };

      await api
        .post('/threads')
        .send(data)
        .set('Cookie', `knockoutJwt=${knockoutJwt}`)
        .expect(httpStatus.BAD_REQUEST);
    });

    test('rejects invalid thread title', async () => {
      const data: CreateThreadRequest = {
        title: '',
        content: 'thread post content',
        icon_id: 1,
        subforum_id: 1,
      };

      await api
        .post('/threads')
        .send(data)
        .set('Cookie', `knockoutJwt=${knockoutJwt}`)
        .expect(httpStatus.BAD_REQUEST);
    });

    test('blocks non gold members from adding backgrounds', async () => {
      const data: CreateThreadRequest = {
        title: 'A Thread',
        content: 'thread post content',
        icon_id: 1,
        subforum_id: 1,
        background_url: 'none.webp',
        background_type: 'cover',
      };

      await api
        .post('/threads')
        .send(data)
        .set('Cookie', `knockoutJwt=${knockoutJwt}`)
        .expect(httpStatus.FORBIDDEN);
    });

    test('allows gold members to add backgrounds', async () => {
      await knex('Users')
        .update({
          usergroup: 2,
        })
        .where('id', TEST_USER_ID);

      const data: CreateThreadRequest = {
        title: 'A Thread',
        content: 'thread post content',
        icon_id: 1,
        subforum_id: 1,
        background_url: 'none.webp',
        background_type: 'cover',
      };

      const expectedResponse: NewThread = {
        id: 1,
        title: 'A Thread',
        iconId: 1,
        userId: TEST_USER_ID,
        subforumId: 1,
        createdAt: null,
        updatedAt: null,
        deletedAt: null,
        locked: false,
        pinned: false,
        backgroundType: 'cover',
        backgroundUrl: 'none.webp',
      };

      await api
        .post('/threads')
        .send(data)
        .set('Cookie', `knockoutJwt=${knockoutJwt}`)
        .expect((res) => {
          createdThreadId = res.body.id;
          expectedResponse.id = res.body.id;
          expectedResponse.createdAt = res.body.createdAt;
          expectedResponse.updatedAt = res.body.updatedAt;
        })
        .expect(httpStatus.CREATED, expectedResponse);

      await knex('Users')
        .update({
          usergroup: 1,
        })
        .where('id', TEST_USER_ID);
    });

    describe('blocks new users from creating threads', () => {
      const newUserJwt = jwt.sign({ id: TEST_USER_ID + 1 }, JWT_SECRET, {
        algorithm: 'HS256',
        expiresIn: '2 weeks',
      });

      beforeAll(async () => {
        await knex('Users').insert({
          id: TEST_USER_ID + 1,
          username: 'Test User',
          avatar_url: 'none.webp',
          usergroup: 1,
          created_at: new Date(),
          updated_at: new Date(),
        });
      });

      afterAll(async () => {
        await knex.raw('SET FOREIGN_KEY_CHECKS=0');
        await knex('Users')
          .where('id', TEST_USER_ID + 1)
          .delete();
        await knex.raw('SET FOREIGN_KEY_CHECKS=1');
      });

      test('in a restricted subforum', async () => {
        const data: CreateThreadRequest = {
          title: 'A Thread',
          content: 'thread post content',
          icon_id: 1,
          subforum_id: minAccountAgeSubforums[0],
        };

        await api
          .post('/threads')
          .send(data)
          .set('Cookie', `knockoutJwt=${newUserJwt}`)
          .expect(httpStatus.FORBIDDEN);
      });

      test('when raid mode is on', async () => {
        redis.set(RAID_MODE_KEY, 'true');
        const data: CreateThreadRequest = {
          title: 'A Thread',
          content: 'thread post content',
          icon_id: 1,
          subforum_id: 1,
        };

        await api
          .post('/threads')
          .send(data)
          .set('Cookie', `knockoutJwt=${newUserJwt}`)
          .expect(httpStatus.FORBIDDEN);

        redis.del(RAID_MODE_KEY);
      });
    });
  });

  describe('GET /:id/page?', () => {
    const threadDate = new Date();

    beforeAll(async () => {
      await knex('Users').insert({
        id: TEST_USER_ID,
        username: 'Test User',
        avatar_url: 'none.webp',
        usergroup: 1,
        created_at: new Date(2020, 1, 1),
        updated_at: new Date(2020, 1, 1),
      });

      await knex('Threads').insert({
        id: TEST_THREAD_ID,
        title: 'A Thread',
        icon_id: 1,
        user_id: TEST_USER_ID,
        subforum_id: 1,
        created_at: threadDate,
        updated_at: threadDate,
        deleted_at: null,
        locked: false,
        pinned: false,
        background_type: null,
        background_url: null,
      });

      await knex('Posts').insert({
        id: 1000,
        content: 'New post.',
        user_id: TEST_USER_ID,
        thread_id: TEST_THREAD_ID,
        thread_post_number: 1,
        created_at: new Date(),
        updated_at: new Date(),
        app_name: 'knockout.chat',
      });

      await knex('Posts').insert({
        id: 1001,
        content: 'New post.',
        user_id: TEST_USER_ID,
        thread_id: TEST_THREAD_ID,
        thread_post_number: 2,
        created_at: new Date(),
        updated_at: new Date(),
        app_name: 'knockout.chat',
      });
      await knex('Posts').insert({
        id: 1002,
        content: 'New post.',
        user_id: TEST_USER_ID,
        thread_id: TEST_THREAD_ID,
        thread_post_number: 3,
        created_at: new Date(),
        updated_at: new Date(),
        app_name: 'knockout.chat',
      });
      await knex('Posts').insert({
        id: 1003,
        content: 'New post.',
        user_id: TEST_USER_ID,
        thread_id: TEST_THREAD_ID,
        thread_post_number: 4,
        created_at: new Date(),
        updated_at: new Date(),
        app_name: 'knockout.chat',
      });
    });

    afterAll(async () => {
      await knex.raw('SET FOREIGN_KEY_CHECKS=0');
      await knex('Threads').where('id', TEST_THREAD_ID).delete();
      await knex('Users').where('id', TEST_USER_ID).delete();
      await knex('Posts').where('id', 1000).delete();
      await knex('Posts').where('id', 1001).delete();
      await knex('Posts').where('id', 1002).delete();
      await knex('Posts').where('id', 1003).delete();
      await knex.raw('SET FOREIGN_KEY_CHECKS=1');
    });

    test('retrieves a thread', async () => {
      const expectedResponse = {
        id: TEST_THREAD_ID,
        title: 'A Thread',
        iconId: 1,
        subforumId: 1,
        deleted: false,
        locked: false,
        pinned: false,
        subscribed: false,
        lastPost: {
          id: 1003,
          thread: TEST_THREAD_ID,
          page: 1,
          content: 'New post.',
          user: {
            id: TEST_USER_ID,
          },
          ratings: [],
          bans: [],
          threadPostNumber: 4,
          countryName: null,
          appName: 'knockout.chat',
        },
        backgroundUrl: null,
        backgroundType: null,
        user: {
          id: TEST_USER_ID,
          username: 'Test User',
          usergroup: 1,
          avatarUrl: 'none.webp',
          backgroundUrl: '',
          banned: false,
          isBanned: false,
        },
        postCount: 4,
        recentPostCount: 0,
        unreadPostCount: 0,
        readThreadUnreadPosts: 0,
        read: false,
        subforum: {
          id: 1,
        },
        tags: [],
        totalPosts: 4,
        currentPage: 1,
        posts: [
          {
            id: 1000,
            thread: TEST_THREAD_ID,
            page: 1,
            content: 'New post.',
            user: {
              id: TEST_USER_ID,
            },
            ratings: [],
            bans: [],
            threadPostNumber: 1,
            countryName: null,
            appName: 'knockout.chat',
          },
          {
            id: 1001,
            thread: TEST_THREAD_ID,
            page: 1,
            content: 'New post.',
            user: {
              id: TEST_USER_ID,
            },
            ratings: [],
            bans: [],
            threadPostNumber: 2,
            countryName: null,
            appName: 'knockout.chat',
          },
          {
            id: 1002,
            thread: TEST_THREAD_ID,
            page: 1,
            content: 'New post.',
            user: {
              id: TEST_USER_ID,
            },
            ratings: [],
            bans: [],
            threadPostNumber: 3,
            countryName: null,
            appName: 'knockout.chat',
          },
          {
            id: 1003,
            thread: TEST_THREAD_ID,
            page: 1,
            content: 'New post.',
            user: {
              id: TEST_USER_ID,
            },
            ratings: [],
            bans: [],
            threadPostNumber: 4,
            countryName: null,
            appName: 'knockout.chat',
          },
        ],
      };

      const response = await api.get(`/threads/${TEST_THREAD_ID}`).expect(httpStatus.OK);

      expect(response.body).toMatchObject(expectedResponse);
    });

    test('does not retrieve deleted threads', async () => {
      await knex('Threads').update({ deleted_at: new Date() }).where('id', TEST_THREAD_ID);
      redis.del(`thread-${TEST_THREAD_ID}`);

      await api.get(`/threads/${TEST_THREAD_ID}`).expect(httpStatus.NOT_FOUND);

      await knex('Threads').update({ deleted_at: null }).where('id', TEST_THREAD_ID);
      redis.del(`thread-${TEST_THREAD_ID}`);
    });

    test('retrieves deleted threads if the user is a moderator', async () => {
      await knex('Threads').update({ deleted_at: new Date() }).where('id', TEST_THREAD_ID);
      redis.del(`thread-${TEST_THREAD_ID}`);
      await knex('Users').update({ usergroup: MODERATOR_GROUPS[0] }).where('id', TEST_USER_ID);

      await api
        .get(`/threads/${TEST_THREAD_ID}`)
        .set('Cookie', `knockoutJwt=${knockoutJwt}`)
        .expect(httpStatus.OK);

      await knex('Users').update({ usergroup: 1 }).where('id', TEST_USER_ID);
      await knex('Threads').update({ deleted_at: null }).where('id', TEST_THREAD_ID);
      redis.del(`thread-${TEST_THREAD_ID}`);
      redis.del(`user-${TEST_USER_ID}`);
    });

    describe('does not retrieve threads in a restricted subforum', () => {
      beforeAll(async () => {
        await knex('Threads')
          .update({ subforum_id: minAccountAgeSubforums[0] })
          .where('id', TEST_THREAD_ID);
        redis.del(`thread-${TEST_THREAD_ID}`);

        await knex('Users').update({ created_at: new Date() }).where('id', TEST_USER_ID);
        redis.del(`user-${TEST_USER_ID}`);
      });

      afterAll(async () => {
        await knex('Threads').update({ subforum_id: 1 }).where('id', TEST_THREAD_ID);
        redis.del(`thread-${TEST_THREAD_ID}`);

        await knex('Users')
          .update({ created_at: new Date(2020, 1, 1) })
          .where('id', TEST_USER_ID);
        redis.del(`user-${TEST_USER_ID}`);
      });

      test('for logged out users', async () => {
        await api.get(`/threads/${TEST_THREAD_ID}`).expect(httpStatus.FORBIDDEN);
      });

      test('for new users', async () => {
        await api
          .get(`/threads/${TEST_THREAD_ID}`)
          .set('Cookie', `knockoutJwt=${knockoutJwt}`)
          .expect(httpStatus.FORBIDDEN);
      });
    });
  });

  describe('latest and popular endpoints', () => {
    beforeAll(async () => {
      await knex('Users').insert({
        id: TEST_USER_ID,
        username: 'Test User',
        avatar_url: 'none.webp',
        usergroup: 1,
        created_at: new Date(2020, 1, 1),
        updated_at: new Date(2020, 1, 1),
      });

      await knex('Threads').insert({
        id: TEST_THREAD_ID,
        title: 'A Thread',
        icon_id: 1,
        user_id: TEST_USER_ID,
        subforum_id: 1,
        created_at: new Date(),
        updated_at: new Date(),
        deleted_at: null,
        locked: false,
        pinned: false,
        background_type: null,
        background_url: null,
      });
    });

    afterAll(async () => {
      await knex.raw('SET FOREIGN_KEY_CHECKS=0');
      await knex('Threads').where('id', TEST_THREAD_ID).delete();
      await knex('Users').where('id', TEST_USER_ID).delete();
      await knex.raw('SET FOREIGN_KEY_CHECKS=1');
    });

    test('GET /latest retrieves latest threads', async () => {
      await api.get('/threads/latest').expect(httpStatus.OK);
    });

    test('GET /popular retrieves popular threads', async () => {
      await api.get('/threads/popular').expect(httpStatus.OK);
    });
  });

  describe('PUT', () => {
    beforeAll(async () => {
      await knex('Users').insert({
        id: TEST_USER_ID,
        username: 'Test User',
        avatar_url: 'none.webp',
        usergroup: 1,
        created_at: new Date(2020, 1, 1),
        updated_at: new Date(2020, 1, 1),
      });

      await knex('Threads').insert({
        id: TEST_THREAD_ID,
        title: 'A Thread',
        icon_id: 1,
        user_id: TEST_USER_ID,
        subforum_id: 1,
        created_at: new Date(),
        updated_at: new Date(),
        deleted_at: null,
        locked: false,
        pinned: false,
        background_type: null,
        background_url: null,
      });
    });

    beforeEach(async () => {
      await knex('Threads')
        .update({
          title: 'A Thread',
          icon_id: 1,
          user_id: TEST_USER_ID,
          subforum_id: 1,
          created_at: new Date(),
          updated_at: new Date(),
          deleted_at: null,
          locked: false,
          pinned: false,
          background_type: null,
          background_url: null,
        })
        .where('id', TEST_THREAD_ID);
    });

    afterEach(async () => {
      await knex('Threads')
        .update({
          title: 'A Thread',
          icon_id: 1,
          user_id: TEST_USER_ID,
          subforum_id: 1,
          created_at: new Date(),
          updated_at: new Date(),
          deleted_at: null,
          locked: false,
          pinned: false,
          background_type: null,
          background_url: null,
        })
        .where('id', TEST_THREAD_ID);
      redis.del(`thread-${TEST_THREAD_ID}`);
    });

    afterAll(async () => {
      await knex.raw('SET FOREIGN_KEY_CHECKS=0');
      await knex('Threads').where('id', TEST_THREAD_ID).delete();
      await knex('Users').where('id', TEST_USER_ID).delete();
      await knex.raw('SET FOREIGN_KEY_CHECKS=1');
    });

    describe('/:id', () => {
      test('blocks logged out users', async () => {
        const data: UpdateThreadRequest = {
          title: 'New title',
        };

        await api.put(`/threads/${TEST_THREAD_ID}`).send(data).expect(httpStatus.UNAUTHORIZED);
      });

      describe('the thread creator', () => {
        test('can update the title of a thread', async () => {
          const data: UpdateThreadRequest = {
            title: 'New title',
          };

          const response = await api
            .put(`/threads/${TEST_THREAD_ID}`)
            .send(data)
            .set('Cookie', `knockoutJwt=${knockoutJwt}`)
            .expect(httpStatus.OK);

          expect(response.body.title).toEqual('New title');
        });

        test('cannot update a locked thread', async () => {
          await knex('Threads').update({ locked: true }).where('id', TEST_THREAD_ID);

          const data: UpdateThreadRequest = {
            title: 'New title',
          };

          await api
            .put(`/threads/${TEST_THREAD_ID}`)
            .send(data)
            .set('Cookie', `knockoutJwt=${knockoutJwt}`)
            .expect(httpStatus.FORBIDDEN);
        });

        test('cannot move the thread to a different subforum', async () => {
          const data: UpdateThreadRequest = {
            subforum_id: 2,
          };

          await api
            .put(`/threads/${TEST_THREAD_ID}`)
            .send(data)
            .set('Cookie', `knockoutJwt=${knockoutJwt}`)
            .expect(httpStatus.FORBIDDEN);
        });

        test('cannot update the background of the thread', async () => {
          const data: UpdateThreadRequest = {
            background_url: 'none.webp',
            background_type: 'cover',
          };

          await api
            .put(`/threads/${TEST_THREAD_ID}`)
            .send(data)
            .set('Cookie', `knockoutJwt=${knockoutJwt}`)
            .expect(httpStatus.FORBIDDEN);
        });

        test('can update the background of the thread as a gold member', async () => {
          await knex('Users')
            .update({ usergroup: GOLD_MEMBER_GROUPS[0] })
            .where('id', TEST_USER_ID);

          const data: UpdateThreadRequest = {
            background_url: 'none.webp',
            background_type: 'cover',
          };

          await api
            .put(`/threads/${TEST_THREAD_ID}`)
            .send(data)
            .set('Cookie', `knockoutJwt=${knockoutJwt}`)
            .expect(httpStatus.OK);

          await knex('Users').update({ usergroup: 1 }).where('id', TEST_USER_ID);
        });
      });

      describe('a non thread creator', () => {
        const newUserJwt = jwt.sign({ id: TEST_USER_ID + 1 }, JWT_SECRET, {
          algorithm: 'HS256',
          expiresIn: '2 weeks',
        });

        beforeAll(async () => {
          await knex('Users').insert({
            id: TEST_USER_ID + 1,
            username: 'Test User',
            avatar_url: 'none.webp',
            usergroup: 1,
            created_at: new Date(),
            updated_at: new Date(),
          });
        });

        afterAll(async () => {
          await knex.raw('SET FOREIGN_KEY_CHECKS=0');
          await knex('Users')
            .where('id', TEST_USER_ID + 1)
            .delete();
          await knex.raw('SET FOREIGN_KEY_CHECKS=1');
        });

        test('cannot update the thread', async () => {
          const data: UpdateThreadRequest = {
            title: 'New title',
          };

          await api
            .put(`/threads/${TEST_THREAD_ID}`)
            .send(data)
            .set('Cookie', `knockoutJwt=${newUserJwt}`)
            .expect(httpStatus.FORBIDDEN);
        });

        describe('who is a moderator', () => {
          beforeAll(async () => {
            await knex('Users')
              .update({ usergroup: MODERATOR_GROUPS[0] })
              .where('id', TEST_USER_ID + 1);
          });

          afterAll(async () => {
            await knex('Users')
              .update({ usergroup: 1 })
              .where('id', TEST_USER_ID + 1);
          });

          test('can update the title of a thread', async () => {
            const data: UpdateThreadRequest = {
              title: 'New title',
            };

            const response = await api
              .put(`/threads/${TEST_THREAD_ID}`)
              .send(data)
              .set('Cookie', `knockoutJwt=${newUserJwt}`)
              .expect(httpStatus.OK);

            expect(response.body.title).toEqual('New title');
          });

          test('cannot update a locked thread', async () => {
            await knex('Threads').update({ locked: true }).where('id', TEST_THREAD_ID);

            const data: UpdateThreadRequest = {
              title: 'New title',
            };

            await api
              .put(`/threads/${TEST_THREAD_ID}`)
              .send(data)
              .set('Cookie', `knockoutJwt=${newUserJwt}`)
              .expect(httpStatus.FORBIDDEN);
          });

          test('can move the thread to a different subforum', async () => {
            const data: UpdateThreadRequest = {
              subforum_id: 2,
            };

            const response = await api
              .put(`/threads/${TEST_THREAD_ID}`)
              .send(data)
              .set('Cookie', `knockoutJwt=${newUserJwt}`)
              .expect(httpStatus.OK);

            expect(response.body.subforum_id).toEqual(2);
          });

          test('can update the background of the thread', async () => {
            const data: UpdateThreadRequest = {
              background_url: 'none.webp',
              background_type: 'cover',
            };

            const response = await api
              .put(`/threads/${TEST_THREAD_ID}`)
              .send(data)
              .set('Cookie', `knockoutJwt=${newUserJwt}`)
              .expect(httpStatus.OK);

            expect(response.body.background_url).toEqual('none.webp');
            expect(response.body.background_type).toEqual('cover');
          });
        });
      });
    });

    describe('/:id/tags', () => {
      beforeAll(async () => {
        await knex('Tags').insert({
          id: 100,
          name: 'Shoes',
          created_at: new Date(),
          deleted_at: new Date(),
        });

        await knex('Tags').insert({
          id: 101,
          name: 'Bread',
          created_at: new Date(),
          deleted_at: new Date(),
        });
      });

      afterAll(async () => {
        await knex.raw('SET FOREIGN_KEY_CHECKS=0');
        await knex('ThreadTags').where('thread_id', TEST_THREAD_ID).delete();
        await knex('Tags').where('id', 100).delete();
        await knex('Tags').where('id', 101).delete();
        await knex.raw('SET FOREIGN_KEY_CHECKS=1');
      });

      beforeEach(async () => {
        await knex('Users').update({ usergroup: 1 }).where('id', TEST_USER_ID);
      });

      test('blocks logged out users', async () => {
        const data: UpdateThreadTagsRequest = {
          tag_ids: [1, 2],
        };

        await api.put(`/threads/${TEST_THREAD_ID}/tags`).send(data).expect(httpStatus.UNAUTHORIZED);
      });

      test('blocks non-moderators', async () => {
        const data: UpdateThreadTagsRequest = {
          tag_ids: [100, 101],
        };

        await api
          .put(`/threads/${TEST_THREAD_ID}/tags`)
          .send(data)
          .set('Cookie', `knockoutJwt=${knockoutJwt}`)
          .expect(httpStatus.FORBIDDEN);
      });

      test('allows moderators to update thread tags', async () => {
        await knex('Users').update({ usergroup: MODERATOR_GROUPS[0] }).where('id', TEST_USER_ID);

        const data: UpdateThreadTagsRequest = {
          tag_ids: [100, 101],
        };

        await api
          .put(`/threads/${TEST_THREAD_ID}/tags`)
          .send(data)
          .set('Cookie', `knockoutJwt=${knockoutJwt}`)
          .expect(httpStatus.OK);

        await knex('Users').update({ usergroup: 1 }).where('id', TEST_USER_ID);
      });
    });
  });
});
