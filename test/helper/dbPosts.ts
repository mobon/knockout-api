import knex from '../../src/services/knex';

const insertThreads = async (data: any) => {
  const dataArray = Array.isArray(data) ? data : [data];

  await knex('Threads').insert(dataArray);
};

const deleteThreads = async () => {
  await knex.raw('SET FOREIGN_KEY_CHECKS=0');
  await knex.raw('TRUNCATE TABLE Threads');
  await knex.raw('SET FOREIGN_KEY_CHECKS=1');
};

export { insertThreads, deleteThreads };
