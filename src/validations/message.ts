import { MESSAGE_CHARACTER_LIMIT, NEWLINE_MULTIPLIER } from '../constants/limits';

/**
 * @returns validation passed?
 */
const validateMessageLength = (content) => {
  const postContent = content.trim();
  if (!postContent.length) {
    return false;
  }

  const lineBreakAdjustment = (postContent.match(/\n/g) || '').length * NEWLINE_MULTIPLIER;
  if (postContent.length + lineBreakAdjustment > MESSAGE_CHARACTER_LIMIT) {
    return false;
  }
  if (postContent.length < 1) {
    return false;
  }

  return true;
};

export default validateMessageLength;
