import { validateThreadStatus } from './post';
import { insertThreads, deleteThreads } from '../../test/helper/dbPosts';

describe('Test thread status validation', () => {
  beforeEach(async () => {
    await deleteThreads();
  });

  afterEach(async () => {
    await deleteThreads();
  });

  test('validation passes for a valid thread', async () => {
    await insertThreads({
      title: 'test',
      created_at: new Date(),
      updated_at: new Date(),
    });

    expect(await validateThreadStatus(1)).toBe(true);
  });

  test('validation fails for a locked thread', async () => {
    await insertThreads({
      title: 'test',
      created_at: new Date(),
      updated_at: new Date(),
      locked: true,
    });

    expect(await validateThreadStatus(1)).toBe(false);
  });

  test('validation fails for a deleted thread', async () => {
    await insertThreads({
      title: 'test',
      created_at: new Date(),
      updated_at: new Date(),
      deleted_at: new Date(),
    });

    expect(await validateThreadStatus(1)).toBe(false);
  });

  test('validation fails for a nonexisting thread', async () => {
    expect(await validateThreadStatus(9999)).toBe(false);
  });
});
