import { ArrayMaxSize, IsBoolean, IsInt, IsObject } from 'class-validator';
import Thread from './thread';

export default class ThreadWithLastPost extends Thread {
  @IsBoolean()
  deleted: boolean;

  @IsBoolean()
  locked: boolean;

  @IsBoolean()
  pinned: boolean;

  @ArrayMaxSize(3)
  @IsObject({ each: true })
  tags: Object[];

  @IsObject()
  user: Object;

  @IsObject()
  lastPost: Object;

  @IsInt()
  postCount: number;
}
