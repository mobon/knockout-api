import { IsInt, IsObject, ValidateNested } from 'class-validator';
import ThreadWithLastPost from './threadWithLastPost';
import Viewers from './viewers';

export default class ThreadWithRecentPosts extends ThreadWithLastPost {
  @IsInt()
  recentPostCount: number;

  @IsObject()
  @ValidateNested()
  viewers: Viewers;
}
