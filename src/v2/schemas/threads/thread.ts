import { IsBoolean, IsDateString, IsIn, IsInt, IsString, IsUrl } from 'class-validator';

export default class Thread {
  @IsInt()
  id: number;

  @IsString()
  title: string;

  @IsInt()
  iconId: number;

  @IsInt()
  subforumId: number;

  @IsDateString()
  createdAt: string;

  @IsDateString()
  updatedAt: string;

  @IsDateString()
  deletedAt: string;

  @IsBoolean()
  locked: boolean;

  @IsBoolean()
  pinned: boolean;

  @IsUrl()
  backgroundUrl?: string;

  @IsIn(['cover', 'tiled'])
  backgroundType?: 'cover' | 'tiled';
}
