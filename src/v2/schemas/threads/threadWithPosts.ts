import {
  IsBoolean,
  IsDateString,
  IsInt,
  IsObject,
  IsOptional,
  ValidateNested,
} from 'class-validator';
import ThreadWithLastPost from './threadWithLastPost';
import Viewers from './viewers';

export default class ThreadWithPosts extends ThreadWithLastPost {
  @IsBoolean()
  subscribed: boolean;

  @IsInt()
  subscriptionLastPostNumber: number;

  @IsInt()
  currentPage: number;

  @IsObject()
  subforum: Object;

  @IsObject({ each: true })
  posts: Object[];

  @IsOptional()
  @IsDateString()
  readThreadLastSeen?: string;

  @IsObject()
  @ValidateNested()
  viewers: Viewers;
}
