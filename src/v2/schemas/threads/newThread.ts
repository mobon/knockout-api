import { IsInt } from 'class-validator';
import Thread from './thread';

export default class NewThread extends Thread {
  @IsInt()
  userId: number;
}
