import { ParameterObject } from 'openapi3-ts';
import { OpenAPI } from 'routing-controllers-openapi';

export default function OpenAPIParam(name: string, props: Partial<ParameterObject>) {
  return OpenAPI((schema) => {
    const index = schema.parameters.findIndex((p) => 'name' in p && p.name === name);
    if (index > -1) {
      schema.parameters[index] = <ParameterObject>{
        ...schema.parameters[index],
        ...props,
      };
    }
    return schema;
  });
}
