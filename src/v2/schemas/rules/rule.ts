import { IsDateString, IsIn, IsInt, IsOptional, IsPositive, IsString } from 'class-validator';

export default class Rule {
  @IsInt()
  id: number;

  @IsOptional()
  @IsString()
  @IsIn(['Subforum', 'Thread'])
  rulableType?: 'Subforum' | 'Thread';

  @IsOptional()
  @IsInt()
  rulableId?: number;

  @IsString()
  category: string;

  @IsString()
  title: string;

  @IsPositive()
  @IsInt()
  cardinality: number;

  @IsString()
  description: string;

  @IsDateString()
  createdAt: string;

  @IsDateString()
  updatedAt: string;
}
