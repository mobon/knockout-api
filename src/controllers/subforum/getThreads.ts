import { Request, Response } from 'express';
import httpStatus from 'http-status';
import ResponseStrategy from '../../helpers/responseStrategy';
import knex from '../../services/knex';
import Thread from '../../retriever/thread';
import { getFormattedObject } from '../../retriever/subforum';
import { MODERATOR_GROUPS } from '../../constants/userGroups';
import MIN_ACCT_AGE_SUBFORUM_IDS from '../../constants/minAccountAgeSubforums';
import { isLimitedUser } from '../../validations/user';

const getThreadCount = async (subforumId: number, showDeleted: Boolean) => {
  const threadCount = await knex
    .count({ all: '*' })
    .from('Threads')
    .where('subforum_id', subforumId)
    .where((builder) => {
      if (!showDeleted) {
        builder.whereNull('deleted_at');
      }
    })
    .first();
  return threadCount.all;
};

const getThreadIds = async (subforumId: number, offset: number, showDeleted: Boolean) => {
  const threadIds = await knex
    .select('Threads.id')
    .from('Threads')
    .where((builder) => {
      if (!showDeleted) {
        builder.whereNull('Threads.deleted_at');
      }
      builder.where('Threads.subforum_id', subforumId);
    })
    .orderBy('Threads.pinned', 'desc')
    .orderBy('Threads.updated_at', 'desc')
    .limit(40)
    .offset(offset);

  return threadIds.map((thread) => thread.id);
};

const showDeleted = (req: Request) => {
  if (typeof req.user === 'undefined') return false;
  return MODERATOR_GROUPS.indexOf(req.user.usergroup) > -1;
};

// eslint-disable-next-line import/prefer-default-export
export const getThreads = async (req: Request, res: Response) => {
  const hideNsfw = req.query.hideNsfw || false;
  const limitedUser = req.isLoggedIn && (await isLimitedUser(req.user.id));
  const guestOrLimited = !req.isLoggedIn || limitedUser;

  if (guestOrLimited && MIN_ACCT_AGE_SUBFORUM_IDS.includes(Number(req.params.id))) {
    res.status(httpStatus.FORBIDDEN);
    res.json({});
    return;
  }

  const showAll = showDeleted(req);
  const pageNum = req.params.page ? Number(req.params.page) : 1;
  const offset = pageNum * 40 - 40;
  const subforum = await getFormattedObject(req.params.id);
  const threadCount = await getThreadCount(subforum.id, showAll);
  const threadIds = await getThreadIds(subforum.id, offset, showAll);
  const threadRetriever = new Thread(threadIds, hideNsfw ? [Thread.HIDE_NSFW] : [], {
    userId: req.isLoggedIn ? req.user.id : null,
  });
  const threads = await threadRetriever.get();

  ResponseStrategy.send(res, {
    id: subforum.id,
    name: subforum.name,
    iconId: subforum.icondId,
    createdAt: subforum.createdAt,
    updatedAt: subforum.updatedAt,
    totalThreads: threadCount,
    currentPage: pageNum,
    threads,
  });
};
