/* eslint-disable import/prefer-default-export */
import { Request, Response } from 'express';

import ResponseStrategy from '../../helpers/responseStrategy';
import knex from '../../services/knex';
import { getFormattedObjects as getFormattedThreadObjects } from '../../retriever/thread';
import { getFormattedObjects as getFormattedUserObjects } from '../../retriever/user';
import NSFW_TAG_ID from '../../constants/nsfwTagId';
import MIN_ACCT_AGE_SUBFORUM_IDS from '../../constants/minAccountAgeSubforums';
import { isLimitedUser } from '../../validations/user';

const getSubforums = async (hideNsfw = false, guestOrLimited = false) => {
  const subforumIdentifier = knex.raw('??', ['Subforums.id']);

  const totalThreads = knex('Threads')
    .count({ all: '*' })
    .where('Threads.subforum_id', subforumIdentifier)
    .whereNull('Threads.deleted_at')
    .as('total_threads');

  const totalPosts = knex('Posts')
    .count({ all: '*' })
    .where('Threads.subforum_id', subforumIdentifier)
    .whereNull('Threads.deleted_at')
    .join('Threads', 'Posts.thread_id', 'Threads.id')
    .as('total_posts');

  const lastPost = knex.raw(
    `(
      select
        p1.id
      from (
        select thread_id,
        max(id) as id
      from Posts
      group by thread_id
      ) as p1
      join Threads
        on p1.thread_id = Threads.id
      left outer join ThreadTags
        on p1.thread_id = ThreadTags.thread_id
      where
        Threads.subforum_id = Subforums.id
        and Threads.deleted_at is null
        ${hideNsfw ? ` and (ThreadTags.tag_id != ${NSFW_TAG_ID} or ThreadTags.tag_id is null)` : ''}
      order by p1.id desc limit 1
    ) as last_post_id`
  );

  const subforums = await knex
    .select(
      'id',
      'name',
      'icon_id',
      'created_at',
      'updated_at',
      'description',
      'icon',
      totalThreads,
      totalPosts,
      lastPost
    )
    .orderBy('Subforums.created_at', 'asc')
    .from('Subforums')
    .where((builder) => {
      if (guestOrLimited) {
        builder.whereNotIn('id', MIN_ACCT_AGE_SUBFORUM_IDS);
      }
    });

  return subforums;
};

const getLastPosts = async (postIds) => {
  const lastPosts = await knex
    .select(
      'Posts.id as postId',
      'Posts.user_id as postUserId',
      'Posts.created_at as postCreatedAt',
      'Threads.subforum_id as subforumId',
      'Threads.id as threadId'
    )
    .join('Threads', 'Posts.thread_id', 'Threads.id')
    .whereIn('Posts.id', postIds)
    .from('Posts');

  const users = await getFormattedUserObjects(lastPosts.map((lastPost) => lastPost.postUserId));

  const threads = await getFormattedThreadObjects(lastPosts.map((lastPost) => lastPost.threadId));

  const detailedLastPosts = lastPosts.map((lastPost, index) => ({
    id: lastPost.postId,
    createdAt: lastPost.postCreatedAt,
    user: users[index],
    thread: threads[index],
  }));

  return detailedLastPosts.reduce((list, lastPost) => {
    list[lastPost.thread.subforumId] = lastPost;
    return list;
  }, {});
};

// eslint-disable-next-line import/prefer-default-export
export const getAll = async (req: Request, res: Response) => {
  const hideNsfw = req.query.hideNsfw || false;
  const limitedUser = req.isLoggedIn && (await isLimitedUser(req.user.id));
  const guestOrLimited = !req.isLoggedIn || limitedUser;

  const subforums = await getSubforums(hideNsfw, guestOrLimited);
  const lastPostIds = subforums.map((subforum) => subforum.last_post_id);

  const lastPosts = await getLastPosts(lastPostIds);

  const output = subforums.map((subforum) => ({
    id: subforum.id,
    name: subforum.name,
    iconId: subforum.icon_id,
    createdAt: subforum.created_at,
    updatedAt: subforum.updated_at,
    description: subforum.description,
    icon: subforum.icon,
    totalThreads: subforum.total_threads,
    totalPosts: subforum.total_posts,
    lastPostId: subforum.last_post_id,
    lastPost: lastPosts[subforum.id],
  }));

  const responseBody = { list: output };

  return ResponseStrategy.send(res, responseBody);
};
