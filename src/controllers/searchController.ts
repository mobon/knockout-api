import crypto from 'crypto';
import { Request, Response } from 'express';
import httpStatus from 'http-status';
import knex from '../services/knex';
import redis from '../services/redisClient';

export const query = async (req: Request, res: Response) => {

  const query = req.body.query || null;
  const nonce = crypto.createHash('sha1').update(query).digest('hex');
  const cacheKey = 'search-result-' + nonce;

  if (query == null) {
    res.status(httpStatus.BAD_REQUEST);
    res.json({ error: 'Missing parameters' });
    return;
  }

  if (query.length <= 3) {
    res.status(httpStatus.BAD_REQUEST);
    res.json({ error: 'Query too short' });
    return;
  }

  res.json({ result: nonce });

  // dont bother re-running the search if the results are already in memory.
  let cache = await redis.getAsync(cacheKey);
  if (cache != null) {
    return;
  }

  const results = await knex
    .from('Threads as th')
    .select(
      'th.id as threadId',
      'th.title as threadTitle',
      'th.icon_id as threadIconId'
    )
    .whereRaw('th.title LIKE ?', ['%' + query + '%'])
    .limit(200);

  const data = {
    query: query,
    results: results
  };

  redis.setex(cacheKey, 120, JSON.stringify(data));

};

export const results = async (req: Request, res: Response) => {

  const nonce = req.params.nonce || null;
  const cacheKey = 'search-result-' + nonce;

  if (nonce == null) {
    res.status(httpStatus.BAD_REQUEST);
    res.json({ error: 'Missing parameters' });
    return;
  }

  let results = await redis.getAsync(cacheKey);
  if (results == null) {
    res.status(httpStatus.NOT_FOUND);
    res.json({ error: 'Invalid URL' });
    return;
  }

  res.json(JSON.parse(results));

};