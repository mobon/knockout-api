import { Request, Response } from 'express';
import httpStatus from 'http-status';
import ResponseStrategy from '../helpers/responseStrategy';
import { getFormattedObject as getFormattedUserObject, invalidateObject } from '../retriever/user';
import Thread from '../retriever/thread';
import Post, { getFormattedObjects as getFormattedPostObjects } from '../retriever/post';
import Ban from '../retriever/ban';

import composer from '../helpers/queryComposer';
import datasource from '../models/datasource';
import knex from '../services/knex';
import redis from '../services/redisClient';
import { validateUserFields } from '../validations/user';
import { getMentionsQuery } from './mentionsController';
import { getAlerts } from './alertController';
import { countOpenReportsQuery } from './moderationController';
import ratingList from '../helpers/ratingList.json';
import { MODERATOR_GROUPS } from '../constants/userGroups';
import errorHandler from '../services/errorHandler';
import { RAID_MODE_KEY } from '../constants/adminSettings';

const { User } = datasource().models;

export const store = async (req: Request, res: Response) => {
  try {
    const raidModeEnabled = await redis.getAsync(RAID_MODE_KEY);

    if (raidModeEnabled) {
      return res.sendStatus(httpStatus.FORBIDDEN);
    }

    const user = await User.create({ ...req.body, usergroup: 1 });

    const scope = composer.scope(req, User);
    const options = composer.options(req, User.blockedFields);

    const resultingUser = scope.findOne({ ...options, where: { id: user.id } });

    return resultingUser;
  } catch (exception) {
    res.status(httpStatus.UNPROCESSABLE_ENTITY);
    return res.json({ error: 'An error has occurred.' });
  }
};

/**
 * Send the loaded user data to the client
 */
export const index = async (req: Request, res: Response) => {
  try {
    if (req.isLoggedIn) {
      res.status(httpStatus.OK);
      res.json({ user: req.user });
    } else {
      res.status(httpStatus.UNAUTHORIZED);
      res.json({ error: 'You are not logged in.' });
    }
  } catch (exception) {
    res.status(httpStatus.UNPROCESSABLE_ENTITY);
    res.json({ error: 'An error has ocurred. ' });
  }
};

export const update = async (req: Request, res: Response) => {
  try {
    const user = await knex.select('*').from('Users').where({ id: req.user.id }).first();

    // shitty way of making sure users can only change what we want
    let fields: { username?: string; avatar_url?: string; background_url?: string } = {};
    if (req.body.username && !user.username) {
      const tentativeUsername = req.body.username.trim();

      if (
        tentativeUsername.length < 3 ||
        !/^[\w\-\_\s\á\é\ó\ú\ã\ê\ô\ç\!\?\$\#\&]+$/.test(tentativeUsername)
      ) {
        throw new Error('This username is not valid.');
      }

      const existingUserCheck = await knex
        .select('*')
        .from('Users')
        .where({ username: tentativeUsername })
        .first();

      if (existingUserCheck && existingUserCheck.length !== 0) {
        throw new Error('This username has already been used.');
      }

      fields = { ...fields, username: tentativeUsername };
    }
    if (
      req.body.avatar_url &&
      (req.body.avatar_url === `${user.id}.webp` || req.body.avatar_url === 'none.webp')
    ) {
      fields = { ...fields, avatar_url: req.body.avatar_url };
    }
    if (req.body.background_url && req.body.background_url === `${user.id}-bg.webp`) {
      fields = { ...fields, background_url: req.body.background_url };
    }

    // eslint-disable-next-line @typescript-eslint/naming-convention
    const { username, avatar_url, background_url } = fields;

    if (!username && !avatar_url && !background_url) {
      throw new Error('Nothing changed.');
    }

    await knex('Users')
      .update({ ...fields })
      .where({ id: user.id });

    const newUser = await knex.select('*').from('Users').where({ id: req.user.id }).first();

    invalidateObject(user.id);

    res.status(httpStatus.OK);
    res.json(newUser);
  } catch (err) {
    errorHandler.respondWithError(httpStatus.BAD_REQUEST, err, res);
  }
};

export const authCheck = async (req: Request, res: Response) => {
  try {
    if (req.user && req.user.isBanned) {
      const bans = await knex('Bans')
        .select('ban_reason', 'Posts.thread_id')
        .leftJoin('Posts', 'Bans.post_id', 'Posts.id')
        .where({
          'Bans.user_id': req.user.id,
        })
        .orderBy('Bans.created_at', 'desc')
        .limit(1);

      const { ban_reason: banMessage, thread_id: threadId } = bans[0];

      if (banMessage) {
        res.status(httpStatus.OK);
        return res.json({ banMessage, threadId });
      }

      throw new Error('Your authcheck failed.');
    }
    if (!req.isLoggedIn) {
      throw new Error('Bad user login on userController.authCheck');
    }
    if (req.user == null || typeof req.user.id === 'undefined') {
      throw new Error('Invalid user.');
    }

    //
    // Gold Membership product check
    // should only run for users with usergroup 1 or 2
    // (so only blue or gold members)
    // NOTE:
    // Uncomment this and fix logic when gold membership
    // is re-implemented

    // we cache this so we don't check it every single time a user opens a tab
    // we don't need this info to be extremely fresh since it's quite inconsequential

    // const cached = await redis.getAsync(`product-gold-membership-user-${req.user.id}`);

    // if (!cached && (req.user.usergroup === 1 || req.user.usergroup === 2)) {
    //   const activeGoldMemberships = await knex('ProductSubscriptions')
    //     .select('expires_at', 'Products.name')
    //     .select(knex.raw('NOW()'))
    //     .where('Products.name', 'GoldMembership')
    //     .where('ProductSubscriptions.user_id', req.user.id)
    //     .whereRaw('expires_at > NOW()')
    //     .leftJoin('Products', 'Products.id', 'ProductSubscriptions.product_id');

    //   if (req.user.usergroup === 1 && activeGoldMemberships.length > 0) {
    //     await knex('Users').update('usergroup', 2).where('id', req.user.id);

    //     eventLogController.becomeGoldEvent({
    //       userId: req.user.id,
    //       username: req.user.username,
    //     });
    //   } else if (req.user.usergroup === 2 && activeGoldMemberships.length === 0) {
    //     await knex('Users').update('usergroup', 1).where('id', req.user.id);

    //     eventLogController.loseGoldEvent({
    //       userId: req.user.id,
    //       username: req.user.username,
    //     });
    //   }

    //   // cache this for 5 minutes with an arbitrary value
    //   redis.setex(`product-gold-membership-user-${req.user.id}`, 300, 'true');
    // }

    res.status(httpStatus.OK);
    return res.send('OK');
  } catch (err) {
    return errorHandler.respondWithError(httpStatus.OK, err, res, 'Your authcheck failed');
  }
};

export const getUserBans = async (req: Request, res: Response) => {
  try {
    if (!req.isLoggedIn) {
      throw new Error('Bad user login on postController.store');
    }
    if (!validateUserFields(req.user)) {
      throw new Error('Invalid user.');
    }
    if (req.user.isBanned) {
      throw new Error('User is banned.');
    }
    if (!req.params.id) {
      throw new Error('No user ID given.');
    }

    const result = await knex('Bans').where({ user_id: req.params.id, deleted_at: null });

    res.status(httpStatus.OK);
    res.json(result);
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};

export const getThreads = async (req: Request, res: Response) => {
  const userId = req.params.id;
  const offset = req.params.page ? Number(req.params.page) * 40 - 40 : 0;
  const hideNsfw = req.query.hideNsfw || false;

  const userLookup = await knex('Users')
    .select('id')
    .where('id', userId)
    .where('deleted_at', null)
    .first();

  if (typeof userLookup === 'undefined') {
    res.status(httpStatus.NOT_FOUND);
    return res.json({ error: 'Not found.' });
  }

  const allThreads = await knex
    .from('Threads as t')
    .count('t.id as count')
    .where('user_id', userId)
    .whereRaw('t.deleted_at is null')
    .first();

  const query = await knex
    .from('Threads as t')
    .select('t.id')
    .where('user_id', userId)
    .whereRaw('t.deleted_at is null')
    .orderBy('t.id', 'desc')
    .limit(40)
    .offset(offset);

  const threadIds = query.map((thread) => thread.id);

  const flags = [Thread.RETRIEVE_SHALLOW, Thread.INCLUDE_LAST_POST];

  if (hideNsfw) {
    flags.push(Thread.HIDE_NSFW);
    flags.push(Thread.INCLUDE_TAGS);
  }

  const threadRetriever = new Thread(threadIds, flags);
  const threads = await threadRetriever.get();
  return ResponseStrategy.send(res, {
    totalThreads: allThreads.count,
    currentPage: Number(req.params.page) || 1,
    threads,
  });
};

export const getPosts = async (req: Request, res: Response) => {
  const userId = req.params.id;
  const offset = req.params.page ? Number(req.params.page) * 40 - 40 : 0;
  const hideNsfw = req.query.hideNsfw || false;

  const userLookup = await knex('Users')
    .select('id')
    .where('id', userId)
    .where('deleted_at', null)
    .first();

  if (typeof userLookup === 'undefined') {
    res.status(httpStatus.NOT_FOUND);
    return res.json({ error: 'Not found.' });
  }

  const allPosts = await knex
    .from('Posts as p')
    .count('p.id as count')
    .where('user_id', userId)
    .first();

  const query = await knex
    .from('Posts as p')
    .select('p.id')
    .where('user_id', userId)
    .orderBy('p.id', 'desc')
    .limit(40)
    .offset(offset);

  const postIds = query.map((post) => post.id);

  const userIsMod = MODERATOR_GROUPS.includes(userLookup.usergroup);
  const flags = [Post.INCLUDE_THREAD];
  if (!userIsMod) {
    flags.push(Post.HIDE_DELETED);
  }

  if (hideNsfw) {
    flags.push(Post.HIDE_NSFW);
  }

  const posts = await getFormattedPostObjects(postIds, flags);
  return ResponseStrategy.send(res, {
    totalPosts: allPosts.count,
    currentPage: Number(req.params.page) || 1,
    posts,
  });
};

export const getBans = async (req: Request, res: Response) => {
  const userId = req.params.id;

  const userLookup = await knex('Users')
    .select('id')
    .where('id', userId)
    .where('deleted_at', null)
    .first();

  if (typeof userLookup === 'undefined') {
    res.status(httpStatus.NOT_FOUND);
    return res.json({ error: 'Not found.' });
  }

  const query = await knex
    .from('Bans as b')
    .select('b.id')
    .where('user_id', userId)
    .orderBy('b.id', 'desc');

  const banIds = query.map((ban) => ban.id);

  const banRetriever = new Ban(banIds, [Ban.INCLUDE_POST, Ban.INCLUDE_THREAD]);
  const bans = await banRetriever.get();
  return ResponseStrategy.send(res, bans);
};

export const show = async (req: Request, res: Response) => {
  const userId = req.params.id;

  const userLookup = await knex('Users')
    .select('id')
    .where('id', userId)
    .where('deleted_at', null)
    .first();

  if (typeof userLookup === 'undefined') {
    res.status(httpStatus.NOT_FOUND);
    return res.json({ error: 'Not found.' });
  }

  const user = await getFormattedUserObject(Number(userId));
  return ResponseStrategy.send(res, user);
};

export const syncData = async (req: Request, res: Response) => {
  try {
    if (!req.isLoggedIn) {
      throw new Error('Bad user login for user data sync.');
    }
    if (!req.user) {
      throw new Error('Invalid user.');
    }

    interface UserDataSyncResponse {
      id: number;
      username: string;
      usergroup: number;
      isBanned: boolean;
      createdAt: Date;
      banInfo?: { banMessage: string; threadId: number };
      avatarUrl: string;
      backgroundUrl: string;
      subscriptions?: Array<object>;
      mentions?: Array<object>;
      reports?: number;
    }
    const userDataSyncResponse: UserDataSyncResponse = {
      id: req.user.id,
      username: req.user.username,
      usergroup: req.user.usergroup,
      isBanned: req.user.isBanned,
      avatarUrl: req.user.avatar_url,
      backgroundUrl: req.user.background_url,
      createdAt: req.user.createdAt,
    };

    if (req.user.isBanned) {
      const bans = await knex('Bans')
        .select('ban_reason', 'Posts.thread_id')
        .leftJoin('Posts', 'Bans.post_id', 'Posts.id')
        .where({
          'Bans.user_id': req.user.id,
        })
        .orderBy('Bans.created_at', 'desc')
        .limit(1);

      const { ban_reason: banMessage, thread_id: threadId } = bans[0];

      if (banMessage) {
        userDataSyncResponse.banInfo = {
          banMessage,
          threadId,
        };
      }
    }

    if (!req.user.isBanned) {
      // get mentions
      userDataSyncResponse.mentions = await getMentionsQuery(req.user.id);

      // get alerts
      userDataSyncResponse.subscriptions = await getAlerts(req.user.id, req.user.usergroup, 1);

      if (MODERATOR_GROUPS.includes(req.user.usergroup)) {
        userDataSyncResponse.reports = await countOpenReportsQuery();
      }
    }

    //
    // cache invalidation for bans
    //
    const cachedUser = await getFormattedUserObject(req.user.id);
    if (cachedUser.banned !== req.user.isBanned) {
      invalidateObject(req.user.id);
    }

    //
    // Gold Membership product check
    // should only run for users with usergroup 1 or 2
    // (so only blue or gold members)
    // NOTE:
    // Uncomment this and fix logic when gold membership
    // is re-implemented

    // we cache this so we don't check it every single time a user opens a tab
    // we don't need this info to be extremely fresh since it's quite inconsequential
    // const cached = await redis.getAsync(`product-gold-membership-user-${req.user.id}`);

    // let hasGoldMembershipInDb = false;

    // if (!cached) {
    //   const activeGoldMemberships = await knex('ProductSubscriptions')
    //     .select('expires_at', 'Products.name')
    //     .select(knex.raw('NOW()'))
    //     .where('Products.name', 'GoldMembership')
    //     .where('ProductSubscriptions.user_id', req.user.id)
    //     .whereRaw('expires_at > NOW()')
    //     .leftJoin('Products', 'Products.id', 'ProductSubscriptions.product_id');

    //   hasGoldMembershipInDb = activeGoldMemberships.length;

    //   if (req.user.usergroup === 1 && activeGoldMemberships.length > 0) {
    //     await knex('Users').update('usergroup', 2).where('id', req.user.id);

    //     userDataSyncResponse.usergroup = 2;

    //     eventLogController.becomeGoldEvent({
    //       userId: req.user.id,
    //       username: req.user.username,
    //     });
    //   } else if (req.user.usergroup === 2 && activeGoldMemberships.length === 0) {
    //     await knex('Users').update('usergroup', 1).where('id', req.user.id);

    //     userDataSyncResponse.usergroup = 1;

    //     eventLogController.loseGoldEvent({
    //       userId: req.user.id,
    //       username: req.user.username,
    //     });
    //   }

    //   // cache this for 5 minutes with an arbitrary value
    //   redis.setex(
    //     `product-gold-membership-user-${req.user.id}`,
    //     300,
    //     JSON.stringify(hasGoldMembershipInDb)
    //   );
    // }

    res.status(httpStatus.OK);
    return res.json(userDataSyncResponse);
  } catch (error) {
    return errorHandler.respondWithError(httpStatus.OK, error, res, 'Your authcheck failed');
  }
};

export const getTopRatings = async (req: Request, res: Response) => {
  try {
    const userId = req.params.id;

    const ratingsHiddenForUser = await knex('Users')
      .select('hide_profile_ratings')
      .where({ id: userId, deleted_at: null });

    if (!ratingsHiddenForUser[0] || ratingsHiddenForUser[0].hide_profile_ratings === 1) {
      res.status(httpStatus.OK);
      return res.json([]);
    }

    const cached = await redis.getAsync(`top-ratings-${userId}`);

    if (cached) {
      res.status(httpStatus.OK);
      return res.send(cached);
    }

    const ratingsLookup = await knex('Posts')
      .select('Ratings.rating_id')
      .count('Ratings.rating_id as count')
      .leftJoin('Threads', 'Posts.thread_id', 'Threads.id')
      .rightJoin('Ratings', 'Ratings.post_id', 'Posts.id')
      .whereRaw(
        `
      Posts.user_id = ?
    `,
        [userId]
      )
      .groupBy('rating_id');

    const ratingsNamed = ratingsLookup.map((val) => {
      if (ratingList[val.rating_id]) {
        return {
          name: ratingList[val.rating_id].short,
          count: val.count,
        };
      }
      return {};
    });

    redis.setex(`top-ratings-${userId}`, 168000, JSON.stringify(ratingsNamed));

    res.status(httpStatus.OK);
    return res.json(ratingsNamed);
  } catch (error) {
    return errorHandler.respondWithError(httpStatus.INTERNAL_SERVER_ERROR, error, res);
  }
};

export const updateProfileRatingsDisplay = async (req: Request, res: Response) => {
  try {
    if (!req.isLoggedIn || !req.user || req.user.isBanned) {
      res.status(httpStatus.FORBIDDEN);
      res.json({ error: 'Forbidden' });
      return;
    }

    const hide = req.body.hideProfileRatings === true;
    const boolValue = hide ? 1 : 0;

    await knex('Users').update({ hide_profile_ratings: boolValue }).where({ id: req.user.id });

    res.status(httpStatus.OK);
    res.json({ message: 'Profile ratings preference saved.' });
  } catch (err) {
    errorHandler.respondWithError(
      httpStatus.BAD_REQUEST,
      err,
      res,
      'You have provided invalid data.'
    );
  }
};

export const getProfileRatingsDisplay = async (req: Request, res: Response) => {
  try {
    if (!req.isLoggedIn || !req.user || req.user.isBanned) {
      res.status(httpStatus.FORBIDDEN);
      res.json({ error: 'Forbidden' });
      return;
    }

    const ratingsHiddenForUser = await knex('Users')
      .select('hide_profile_ratings')
      .where({ id: req.user.id });

    if (!ratingsHiddenForUser[0]) {
      throw new Error('Could not find data for user.');
    }

    res.status(httpStatus.OK);
    res.json({ ratingsHiddenForUser: ratingsHiddenForUser[0].hide_profile_ratings === 1 });
  } catch (err) {
    errorHandler.respondWithError(httpStatus.BAD_REQUEST, err, res, 'Could not find data for user');
  }
};

export const deleteOwnAccount = async (req: Request, res: Response) => {
  try {
    if (!req.isLoggedIn || !req.user || req.user.isBanned) {
      res.status(httpStatus.FORBIDDEN);
      res.json({ error: 'Forbidden' });
      return;
    }

    await knex.raw('call deleteUserById(?)', [req.user.id]);

    await redis.flushall();

    res.status(httpStatus.OK);
    res.json({ message: 'User deleted.' });
  } catch (err) {
    errorHandler.respondWithError(httpStatus.BAD_REQUEST, err, res, 'Could not find data for user');
  }
};
