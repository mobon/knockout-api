import { Request, Response, User } from 'express';
import httpStatus from 'http-status';
import {
  ForbiddenError,
  InternalServerError,
  NotFoundError,
  UnauthorizedError,
} from 'routing-controllers';
import { POSTS_PER_PAGE } from '../../config/server';
import Pagination from '../helpers/PaginationBuilder';
import composer from '../helpers/queryComposer';
import ResponseStrategy from '../helpers/responseStrategy';
import datasource from '../models/datasource';
import ThreadRetriever, {
  getFormattedObject,
  getFormattedObjects,
  invalidateObject,
} from '../retriever/thread';
import knex from '../services/knex';
import redis from '../services/redisClient';
import { validatePostLength } from '../validations/post';
import { validateThreadTitleLength, validateThreadTagCount } from '../validations/thread';
import { validateUserFields, isLimitedUser } from '../validations/user';
import { isProxy } from '../helpers/proxyDetect';
import * as eventLogController from './eventLogController';
import * as postController from './postController';
import { GOLD_MEMBER_GROUPS, MODERATOR_GROUPS } from '../constants/userGroups';
import { RAID_MODE_KEY } from '../constants/adminSettings';
import MIN_ACCT_AGE_SUBFORUM_IDS from '../constants/minAccountAgeSubforums';
import errorHandler from '../services/errorHandler';
import { getFormattedObjects as getPostObjects } from '../retriever/post';
import CreateThreadRequest from '../v2/schemas/threads/createThreadRequest';
import UpdateThreadRequest from '../v2/schemas/threads/updateThreadRequest';
import UpdateThreadTagsRequest from '../v2/schemas/threads/updateThreadTagsRequest';
import { POPULAR_THREAD_VIEWS_KEY } from '../handlers/threadHandler';

const { Thread } = datasource().models;

const POPULAR_THREADS_KEY = 'popular-threads';
const LATEST_THREADS_KEY = 'latest-threads';

export const index = async (req: Request, res: Response) => {
  const scope = composer.scope(req, Thread);
  const options = composer.options(req, Thread.blockedFields);

  options.distinct = true;
  options.col = 'Thread.id';

  try {
    const result = await scope.findAndCountAll(options);

    const pagResult = new Pagination(options, req, result).build();
    redis.setex(composer.keyCache(req), 1, JSON.stringify(pagResult));

    ResponseStrategy.send(res, pagResult, options);
  } catch (error) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, error, res);
  }
};

export const popular = async () => {
  const viewerCacheValue = await redis.zrevrangebyscoreAsync(
    POPULAR_THREAD_VIEWS_KEY,
    'inf',
    '-inf',
    'withscores',
    'limit',
    0,
    20
  );

  const threadViewerMap: object = viewerCacheValue.reduce((map, k, i, res) => {
    if (i % 2 !== 0) {
      map[Number(res[i - 1])] = Number(k);
    }
    return map;
  }, {});

  if (Object.keys(threadViewerMap).length >= 10 && Number(viewerCacheValue[1]) > 0) {
    const ids = Object.keys(threadViewerMap).map(Number);
    const threads = await getFormattedObjects(ids, [
      ThreadRetriever.RETRIEVE_SHALLOW,
      ThreadRetriever.INCLUDE_USER,
      ThreadRetriever.INCLUDE_TAGS,
    ]);

    const threadMap = threads.reduce((map, thread) => {
      map[thread.id] = thread;
      return map;
    }, {});

    const output = [];

    viewerCacheValue.forEach((item, i) => {
      if (i % 2 === 0) {
        output.push({ ...threadMap[item], viewers: { memberCount: threadViewerMap[item] || 0 } });
      }
    });

    return output;
  }

  const threadsCache = await redis.getAsync(POPULAR_THREADS_KEY);

  if (threadsCache) {
    return JSON.parse(threadsCache);
  }

  const query = await knex
    .from('Posts as po')
    .select('th.id', knex.raw('count(po.id) as replyCount'))
    .join('Threads as th', 'po.thread_id', 'th.id')
    .whereRaw(
      'po.created_at >= DATE_SUB(NOW(), INTERVAL 1 DAY) AND locked = 0 AND th.deleted_at IS NULL'
    )
    .groupBy('th.id')
    .orderByRaw('replyCount DESC')
    .limit(20);

  const threadIds = query.map((item) => item.id);

  const threads = await getFormattedObjects(threadIds, [
    ThreadRetriever.RETRIEVE_SHALLOW,
    ThreadRetriever.INCLUDE_USER,
    ThreadRetriever.INCLUDE_TAGS,
    ThreadRetriever.INCLUDE_RECENT_POSTS,
  ]);

  redis.setex(
    POPULAR_THREADS_KEY,
    60,
    JSON.stringify(threads.map((item) => ({ ...item, viewers: undefined })))
  );

  return threads;
};

export const popularThreads = async (req: Request, res: Response) => {
  const threads = await popular();

  return ResponseStrategy.send(res, { list: threads });
};

export const latest = async () => {
  const threadsCache = await redis.getAsync(LATEST_THREADS_KEY);

  if (threadsCache) {
    return JSON.parse(threadsCache);
  }

  const query = await knex
    .from('Threads as th')
    .select('th.id')
    .whereRaw('locked = 0 AND th.deleted_at IS NULL')
    .limit(20)
    .orderByRaw('th.created_at DESC');

  const threadIds = query.map((item) => item.id);

  const threads = await getFormattedObjects(threadIds, []);

  redis.set(LATEST_THREADS_KEY, JSON.stringify(threads));

  return threads;
};

export const latestThreads = async (req: Request, res: Response) => {
  const threads = await latest();
  return ResponseStrategy.send(res, { list: threads });
};

export const store = async (req: Request, res: Response, body: CreateThreadRequest) => {
  const scope = composer.scope(req, Thread);

  if (!validateUserFields(req.user)) {
    throw new UnauthorizedError('Invalid user');
  }
  // validations for a valid post
  if (!validatePostLength(body.content) || !validateThreadTitleLength(body.title)) {
    throw new InternalServerError('Failed validations.');
  }
  const limitedUser = await isLimitedUser(req.user.id);
  if (limitedUser) {
    // are they using a VPN or do they look like they're in a datacenter?
    const ip = req.ipInfo;
    if (isProxy(ip)) {
      console.log('VPN detected', ip);
      throw new InternalServerError('You appear to be using a proxy/VPN.');
    }

    const raidModeEnabled = await redis.getAsync(RAID_MODE_KEY);

    if (raidModeEnabled || MIN_ACCT_AGE_SUBFORUM_IDS.includes(Number(body.subforum_id))) {
      throw new ForbiddenError('Your account is too new to post in this subforum.');
    }
  }

  // check if user is a gold member. if he's not and sent a bg url,
  // boot him.
  const allowedUsergroups = GOLD_MEMBER_GROUPS;
  if (!allowedUsergroups.includes(req.user.usergroup) && body.background_url) {
    throw new ForbiddenError('Not authorized.');
  }

  // TODO: wrap in transaction?
  try {
    const thread = await Thread.create({
      ...body,
      user_id: req.user.id,
    });

    await postController.store(
      { ...req, returnEarly: true, body: { ...body, thread_id: thread.id } } as Request,
      res
    );

    // Insert thread tags
    if (body.tag_ids && body.tag_ids.length > 0) {
      await knex('ThreadTags').insert(
        body.tag_ids.map((tagId) => ({
          tag_id: tagId,
          thread_id: thread.id,
        }))
      );
    }
    redis.del(LATEST_THREADS_KEY);
    return (await scope.findOne({ where: { id: thread.id } })).toJSON();
  } catch (error) {
    throw new InternalServerError('Error creating thread.');
  }
};

export const storeThread = async (req: Request, res: Response) => {
  try {
    if (req.user.isBanned) {
      throw new Error('User is banned');
    }

    if (
      !req.body.title ||
      !validateThreadTitleLength(req.body.title) ||
      !validatePostLength(req.body.content) ||
      !validateThreadTagCount(req.body.tagIds)
    ) {
      throw new Error('Invalid content');
    }

    const result = await store(req, res, {
      ...req.body,
      background_url: req.body.backgroundUrl,
      background_type: req.body.backgroundType,
      tag_ids: req.body.tagIds,
    });

    res.status(httpStatus.CREATED);
    res.json(result);
  } catch (exception) {
    if (exception instanceof InternalServerError) {
      res.status(httpStatus.INTERNAL_SERVER_ERROR);
    } else if (exception instanceof ForbiddenError) {
      res.status(httpStatus.FORBIDDEN);
    } else if (exception instanceof UnauthorizedError) {
      res.status(httpStatus.UNAUTHORIZED);
    } else {
      res.status(httpStatus.UNPROCESSABLE_ENTITY);
    }
    res.json({ error: exception.message });
  }
};

export const update = async (id: number, body: UpdateThreadRequest, user: User) => {
  const allowedUsergroups = MODERATOR_GROUPS;

  const thread = await Thread.findOne({ where: { id } });
  const oldTitle = thread.title;

  // if this user is not a mod and not the author of the thread, he cant edit it
  if (!allowedUsergroups.includes(user.usergroup) && thread.user_id !== user.id) {
    throw new ForbiddenError();
  }

  if (body.title) {
    if (!validateThreadTitleLength(body.title)) {
      throw new ForbiddenError('Invalid thread title');
    }
  }

  // a thread's subforum_id can only be changed by mods
  if (body.subforum_id && !allowedUsergroups.includes(user.usergroup)) {
    throw new ForbiddenError('Not authorized.');
  }

  // check if user is a gold member. if he's not and sent a bg url/type, boot him.
  if (
    (body.background_url || body.background_type) &&
    !GOLD_MEMBER_GROUPS.includes(user.usergroup)
  ) {
    throw new ForbiddenError('Not authorized.');
  }

  if (thread.locked === false) {
    await knex('Threads').where({ id }).update(body);
  } else {
    throw new ForbiddenError("You can't do that, dummy");
  }

  await invalidateObject(id);

  const result = (await Thread.findOne({ where: { id } })).toJSON();

  if (body.title) {
    eventLogController.threadUpdateEvent({
      username: user.username,
      oldTitle,
      newTitle: result.title,
      threadId: id,
      userId: user.id,
      body,
    });
  }

  if (body.background_url) {
    eventLogController.threadBackgroundUpdateEvent({
      username: user.username,
      threadTitle: thread.title,
      threadId: id,
      userId: user.id,
    });
  }

  return result;
};

export const updateThread = async (req: Request, res: Response) => {
  try {
    const result = await update(
      req.body.id,
      {
        ...req.body,
        background_url: req.body.backgroundUrl,
        background_type: req.body.backgroundType,
      },
      req.user
    );

    res.status(httpStatus.OK);
    res.json(result);
  } catch (err) {
    if (err instanceof ForbiddenError) {
      res.status(httpStatus.FORBIDDEN);
      res.json({ error: err.message });
    } else {
      res.status(httpStatus.UNPROCESSABLE_ENTITY).send(err.message);
    }
  }
};

export const destroy = async (req: Request, res: Response) => {
  Thread.destroy({ where: req.params })
    .then(() => res.sendStatus(httpStatus.NO_CONTENT))
    .catch(() => res.status(httpStatus.UNPROCESSABLE_ENTITY));
};

export const count = (req: Request, res: Response) => {
  const options = composer.onlyQuery(req);

  Thread.count(options)
    .then((result) => {
      res.status(httpStatus.OK);
      res.json(result);
    })
    .catch((err) => {
      res.status(httpStatus.UNPROCESSABLE_ENTITY);
      res.json(err.message);
    });
};

export const getPostsAndCount = async (req: Request) => {
  // if no page parameter is provided, default to page 1
  const pageParam: number = Number(req.params.page) || 1;

  // get total posts for this thread
  const postIds = await knex
    .from('Posts as po')
    .select('po.id as id')
    .where('po.thread_id', req.params.id)
    .limit(Number(POSTS_PER_PAGE))
    .offset(Number(POSTS_PER_PAGE) * (pageParam - 1));

  const currentUserGroup = req.user?.usergroup ? req.user.usergroup : 0;
  const userIsMod = MODERATOR_GROUPS.includes(currentUserGroup);

  const flags = [
    ThreadRetriever.RETRIEVE_SHALLOW,
    ThreadRetriever.INCLUDE_LAST_POST,
    ThreadRetriever.INCLUDE_USER,
    ThreadRetriever.INCLUDE_TAGS,
    ThreadRetriever.INCLUDE_SUBFORUM,
  ];

  if (!userIsMod) {
    flags.push(ThreadRetriever.HIDE_DELETED);
  } else {
    flags.push(ThreadRetriever.INCLUDE_VIEWER_USERS);
  }

  const thread = await getFormattedObject(req.params.id, flags);

  if (!thread) {
    throw new NotFoundError();
  }

  const limitedUser = req.isLoggedIn && (await isLimitedUser(req.user.id));
  const guestOrLimited = !req.isLoggedIn || limitedUser;

  if (guestOrLimited && MIN_ACCT_AGE_SUBFORUM_IDS.includes(thread.subforumId)) {
    throw new ForbiddenError();
  }

  if (postIds.length > 0) {
    const posts = await getPostObjects(
      postIds.map((post) => post.id),
      []
    );

    const response: any = {
      ...thread,
      totalPosts: thread.postCount,
      currentPage: pageParam,
      threadBackgroundUrl: thread.backgroundUrl,
      threadBackgroundType: thread.backgroundType,
      posts,
    };

    if (req.isLoggedIn) {
      // get read threads
      const unreadPosts = await knex
        .select('thread_id', 'last_seen as lastSeen')
        .first()
        .from('ReadThreads')
        .whereRaw('ReadThreads.thread_id = ? AND ReadThreads.user_id = ?', [
          req.params.id,
          req.user.id,
        ]);

      if (unreadPosts) {
        response.readThreadLastSeen = unreadPosts.lastSeen;
      }

      // get subscriptions (alerts)
      const subscriptions = await knex
        .select('thread_id', 'last_seen as lastSeen', 'last_post_number as lastPostNumber')
        .first()
        .from('Alerts')
        .whereRaw('Alerts.thread_id = ? AND Alerts.user_id = ?', [req.params.id, req.user.id]);

      response.subscribed = Boolean(subscriptions && subscriptions.thread_id);
      if (response.subscribed) {
        response.subscriptionLastSeen = subscriptions.lastSeen;
        response.subscriptionLastPostNumber = subscriptions.lastPostNumber;
      }
    }

    return response;
  }

  const response = {
    ...thread,
    currentPage: pageParam,
    totalPosts: thread.postCount,
    posts: [],
  };

  return response;
};

export const withPostsAndCount = async (req: Request, res: Response) => {
  try {
    const response = await getPostsAndCount(req);
    return ResponseStrategy.send(res, { ...response, isSubscribedTo: response.subscribed });
  } catch (error) {
    if (error instanceof ForbiddenError) {
      res.status(httpStatus.FORBIDDEN);
      return res.json({ error: error.message });
    }
    if (error instanceof NotFoundError) {
      res.status(httpStatus.NOT_FOUND);
      return res.json({ error: error.message });
    }
    return errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, error, res);
  }
};

export const updateTags = async (id: number, body: UpdateThreadTagsRequest, user: User) => {
  // at this time only mods can edit an existing thread's tags
  if (!MODERATOR_GROUPS.includes(user.usergroup)) {
    throw new ForbiddenError('Invalid user');
  }

  // delete all existing tags for that thread
  await knex('ThreadTags').delete().where({
    thread_id: id,
  });

  try {
    if (body.tag_ids.length > 0) {
      await knex('ThreadTags').insert(
        body.tag_ids.map((tag) => ({
          tag_id: tag,
          thread_id: id,
        }))
      );
    }
  } catch (error) {
    throw new InternalServerError('Error updating thread tags.');
  }

  return invalidateObject(id);
};

export const updateThreadTags = async (req: Request, res: Response) => {
  if (!req.body.threadId || !req.body.tags || req.body.tags.length > 3) {
    res.status(httpStatus.UNPROCESSABLE_ENTITY);
    return res.json({ error: 'Invalid content' });
  }
  try {
    await updateTags(req.body.threadId, { ...req.body, tag_ids: req.body.tags }, req.user);
  } catch (error) {
    if (error instanceof ForbiddenError) {
      res.status(httpStatus.FORBIDDEN);
      res.json({ error: error.message });
    } else {
      res.status(httpStatus.UNPROCESSABLE_ENTITY).send(error.message);
    }
  }

  res.status(httpStatus.OK);
  return res.json({ message: 'Tags updated.' });
};
