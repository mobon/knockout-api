import * as patreon from '@nathanhigh/patreon';

import { Request, Response } from 'express';
import httpStatus from 'http-status';
import knex from '../services/knex';
import { createGoldProductSubscriptionsForUser } from './productController';
import { NODE_ENV, PATREON_CLIENT_ID, PATREON_CLIENT_SECRET } from '../../config/server';
import { createRedirectUrl } from '../helpers/utils';
import errorHandler from '../services/errorHandler';

interface Tier {
  id: number;
  title: string;
}

const tierNames = {
  1: "I'm really helping!",
  2: 'Leave it to me.',
  3: "You're like a little baby.",
  4: 'Watch this.',
};

const getRedirectUrl = (req: Request) => {
  const protocol = NODE_ENV === 'development' ? 'http' : 'https';
  return createRedirectUrl(req, '/link/patreon', protocol);
};

function redirect(req: Request, res: Response) {
  const url = `${
    'https://www.patreon.com/oauth2/authorize' +
    '?response_type=code' +
    '&scope=identity%20campaigns%20campaigns.members%20identity%5Bemail%5D' +
    '&client_id='
  }${encodeURIComponent(PATREON_CLIENT_ID)}&redirect_url=${encodeURIComponent(
    getRedirectUrl(req)
  )}`;
  res.redirect(302, url);
}

async function linkPatreonAccountInDatabase(patreonUserId: number, userId: number) {
  try {
    const existingRecords = await knex('ExternalAccounts').select('user_id', 'id').where({
      provider: 'PATREON',
      external_id: patreonUserId,
    });

    // patreon accounts can only be used by one user
    existingRecords.forEach((row) => {
      if (row.user_id !== userId) {
        throw new Error('Patreon account already used.');
      }
    });

    if (existingRecords.length === 0) {
      await knex('ExternalAccounts').insert({
        provider: 'PATREON',
        user_id: userId,
        external_id: patreonUserId,
      });
    }

    return true;
  } catch (error) {
    return false;
  }
}

async function handleCallback(req: Request, res: Response) {
  try {
    const { code } = req.query;
    const oauth = patreon.oauth(PATREON_CLIENT_ID, PATREON_CLIENT_SECRET);
    const tokens = await oauth.getTokens(code, getRedirectUrl(req));
    const api = await patreon.patreon(tokens.access_token);
    const userInfo = await api('/identity?include=memberships');

    const patreonUserId: number = userInfo.rawJson.data.id;

    const { memberships } = userInfo.rawJson.data.relationships;

    // insert into DB. to be used later
    await linkPatreonAccountInDatabase(patreonUserId, req.user.id);

    // force into checkUserPledgeStatus with true
    if (memberships.data[0]) {
      const patreonMembershipId = memberships.data[0].id;

      const pledgeInfo = await api(
        `/members/${patreonMembershipId}?include=currently_entitled_tiers&fields%5Btier%5D=title`
      );

      const tiers = pledgeInfo.store.graph.tier;

      if (Object.values(tiers).length > 0) {
        const tierTitles = Object.values(tiers).map((el: Tier) => el.title);

        const productIdArray = Array.from(
          Array(Object.values(tierNames).findIndex((el) => el === tierTitles[0]) + 1),
          (e, i) => i + 1
        );

        const callbackFn = (data: { error?: string; message?: string }) => {
          if (data.error) {
            res.status(500);
            return res.send(data.error);
          }

          res.status(200);
          return res.send(data.message);
        };

        createGoldProductSubscriptionsForUser(req.user, productIdArray, callbackFn);
      }
    } else {
      res.status(500);
      res.send('No Patreon membership found.');
    }
  } catch (err) {
    errorHandler.respondWithError(httpStatus.INTERNAL_SERVER_ERROR, err, res);
  }
}

export async function link(req: Request, res: Response) {
  if (req.query.code) {
    return handleCallback(req, res);
  }
  return redirect(req, res);
}

export async function getUserIsLinkedToPatreon(req: Request, res: Response) {
  try {
    const result = await knex('ExternalAccounts')
      .select('id')
      .where('user_id', req.user.id)
      .where('provider', 'PATREON');

    res.status(200);
    res.json({
      userId: req.user.id,
      isLinkedToPatreon: result.length > 0,
    });
  } catch (error) {
    errorHandler.respondWithError(httpStatus.INTERNAL_SERVER_ERROR, error, res);
  }
}
