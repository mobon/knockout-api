import { Request, Response } from 'express';
import httpStatus from 'http-status';
import knex from '../services/knex';
import { invalidateObject as invalidateConversation } from '../retriever/conversation';

import onDuplicateUpdate from '../helpers/onDuplicateUpdate';
import errorHandler from '../services/errorHandler';
import validateMessageLength from '../validations/message';
import { isUserInConversation } from './conversationController';
import { invalidateObject as invalidateMessage, getFormattedObject } from '../retriever/message';

export const store = async (req: Request, res: Response) => {
  try {
    if (!req.isLoggedIn) {
      res.status(httpStatus.UNAUTHORIZED);
      res.json([]);
      return;
    }

    if (!validateMessageLength(req.body.content)) {
      throw new Error('Failed validations.');
    }

    if (!req.body.receivingUserId) {
      throw new Error('Message must have a recipient.');
    }

    let message;
    await knex.transaction(async (trx) => {
      // if no conversation ID is passed,
      // we assume the client wants a new
      // conversation to be created
      let conversationId: number;

      if (req.body.conversationId) {
        conversationId = req.body.conversationId;
        if (!isUserInConversation(conversationId, req.user.id)) {
          res.sendStatus(httpStatus.FORBIDDEN);
          return;
        }
      } else {
        const createResult = await trx('Conversations')
          .insert({})
          .then((results) => results[0]);
        conversationId = createResult;

        // if this user is not part of
        // the conversation already, add them to it
        await onDuplicateUpdate(trx, 'ConversationUsers', {
          user_id: req.user.id,
          conversation_id: conversationId,
        });

        // if the receiving user is not part of
        // the conversation already, add them to it
        //
        // note: if we want
        // more than 2 users per conversation,
        // this controller method / block will need to be updated
        // to accept an array of users
        await onDuplicateUpdate(trx, 'ConversationUsers', {
          user_id: req.body.receivingUserId,
          conversation_id: conversationId,
        });
      }

      // create message
      message = await trx('Messages')
        .insert({
          user_id: req.user.id,
          conversation_id: conversationId,
          content: req.body.content,
        })
        .then((results) => results[0]);

      // update conversation
      await trx('Conversations')
        .where('id', conversationId)
        .update({ latest_message_id: message, updated_at: new Date() });

      // invalidate conversation
      invalidateConversation(conversationId);
    });

    // format message for response
    const formattedMessage = await getFormattedObject(message);

    res.status(httpStatus.CREATED);
    res.json(formattedMessage);
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};

export const update = async (req: Request, res: Response) => {
  try {
    if (!req.isLoggedIn) {
      res.status(httpStatus.UNAUTHORIZED);
      res.json([]);
      return;
    }

    const message = await getFormattedObject(req.params.id);

    if (!isUserInConversation(message.conversationId, req.user.id)) {
      res.sendStatus(httpStatus.FORBIDDEN);
      return;
    }

    await knex('Messages')
      .update({
        read_at: new Date(),
      })
      .where('id', req.params.id);

    await invalidateMessage(req.params.id);

    // format message for response
    const updatedMessage = await getFormattedObject(req.params.id);

    res.status(httpStatus.OK);
    res.json(updatedMessage);
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};
