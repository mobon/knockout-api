/* eslint-disable import/prefer-default-export */

import { Request, Response } from 'express';
import httpStatus from 'http-status';
import onDuplicateUpdate from '../helpers/onDuplicateUpdate';
import ratingList from '../helpers/ratingList.json';
import { invalidateObject } from '../retriever/postRating';
import errorHandler from '../services/errorHandler';
import knex from '../services/knex';

export const store = async (req: Request, res: Response) => {
  try {
    const ratingId = Object.keys(ratingList).find(
      (key) => ratingList[key].short === req.body.rating
    );
    if (!ratingId) {
      throw new Error('Invalid rating.');
    }

    const post = await knex.select('user_id').from('Posts').where('id', req.body.postId);

    if (!post[0]) {
      throw new Error('Invalid post.');
    }
    if (post[0].user_id === req.user.id) {
      throw new Error('Rating yourself is pretty sad.');
    }

    const result = onDuplicateUpdate(knex, 'Ratings', {
      user_id: req.user.id,
      post_id: req.body.postId,
      rating_id: ratingId,
    });

    invalidateObject(req.body.postId);
    res.status(httpStatus.CREATED);
    res.json(result);
  } catch (exception) {
    errorHandler.respondWithError(httpStatus.UNPROCESSABLE_ENTITY, exception, res);
  }
};
