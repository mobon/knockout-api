import AbstractRetriever from './abstractRetriever';
import Thread from './thread';
import knex from '../services/knex';

export default class Alert extends AbstractRetriever {
  private cachePrefix: string = 'alert';

  private user: number;

  public static HIDE_DELETED_THREADS = 1;

  public static HIDE_NSFW = 2;

  constructor(ids: Array<number>, user: number, flags?: Array<number>, args?: Object) {
    super(ids, flags, args);
    this.user = user;
  }

  private static async getAlerts(ids: Array<number>, user: number) {
    const results: any[] = await knex
      .from('Alerts as al')
      .select(
        'al.thread_id as thread_id',
        'al.last_post_number as last_post_number',
        'al.last_seen as last_seen'
      )
      .where('al.user_id', user)
      .whereIn('al.thread_id', ids);

    return results;
  }

  private async getThreads(alerts: Array<any>) {
    const threads = alerts.map((alert) => alert.thread_id);
    const threadFlags = [
      Thread.RETRIEVE_SHALLOW,
      Thread.EXCLUDE_READ_THREADS,
      Thread.INCLUDE_LAST_POST,
      Thread.INCLUDE_USER,
    ];

    if (this.hasFlag(Alert.HIDE_DELETED_THREADS)) {
      threadFlags.push(Thread.HIDE_DELETED);
    }

    if (this.hasFlag(Alert.HIDE_NSFW)) {
      threadFlags.push(Thread.HIDE_NSFW);
      threadFlags.push(Thread.INCLUDE_TAGS);
    }

    const threadRetriever = new Thread(threads, threadFlags);
    const rawThreads = await threadRetriever.get();
    return rawThreads.reduce((list, thread) => {
      list[thread.id] = thread;
      return list;
    }, {});
  }

  private static async getUnreadCount(data) {
    if (data.last_post_number) {
      return data.thread.postCount - data.last_post_number;
    }
    const result = await knex('Posts')
      .count('id as unreadPosts')
      .first()
      .where('thread_id', data.thread_id)
      .where('created_at', '>', data.last_seen);

    return result.unreadPosts;
  }

  private static async getFirstUnreadId(data) {
    let result;
    if (data.last_post_number) {
      result = await knex('Posts')
        .select('id as firstUnreadId')
        .first()
        .where('thread_id', data.thread_id)
        .where('thread_post_number', data.last_post_number + 1);
    } else {
      result = await knex('Posts')
        .select('id as firstUnreadId')
        .first()
        .where('thread_id', data.thread_id)
        .where('created_at', '>', data.last_seen)
        .orderBy('created_at')
        .limit(1);
    }

    if (!result) {
      return -1;
    }

    return result.firstUnreadId;
  }

  private static async format(data): Promise<Object> {
    return {
      threadId: data.thread_id,
      icon_id: data.thread.iconId, // deprecate
      threadIcon: data.thread.iconId,
      threadTitle: data.thread.title,
      threadUser: data.thread.user.id,
      threadLocked: data.thread.locked,
      threadCreatedAt: data.thread.createdAt,
      threadUpdatedAt: data.thread.updatedAt,
      threadDeletedAt: data.thread.deletedAt,
      threadBackgroundUrl: data.thread.backgroundUrl,
      threadBackgroundType: data.thread.backgroundType,
      threadUsername: data.thread.user.username,
      threadUserAvatarUrl: data.thread.user.avatarUrl,
      threadUserUsergroup: data.thread.user.usergroup,
      threadPostCount: data.thread.postCount,
      lastPost: data.thread.lastPost,
      unreadPosts: await Alert.getUnreadCount(data),
      firstUnreadId: await Alert.getFirstUnreadId(data),
    };
  }

  async get() {
    // grab canonical data
    const alerts = await Alert.getAlerts(this.ids, this.user);
    const threads = await this.getThreads(alerts);

    // merge related data in
    alerts.forEach((alert) => {
      alert.thread = threads[alert.thread_id];
    });

    const filteredAlerts = alerts.filter((alert) => alert.thread !== undefined);

    return Promise.all(filteredAlerts.map((alert) => Alert.format(alert)));
  }

  async invalidate() {
    this.ids.map(async (id) => {
      await this.cacheDrop(this.cachePrefix, id);
    });
  }
}

export const invalidateObjects = async (ids: Array<number>, user: number) => {
  const alertRetriever = new Alert(ids, user, []);
  await alertRetriever.invalidate();
};

export const invalidateObject = async (id: number, user: number) => {
  const alertRetriever = new Alert([id], user, []);
  await alertRetriever.invalidate();
};

export const getFormattedObjects = async (
  ids: Array<number>,
  user: number,
  flags: Array<number> = []
) => {
  const alertRetriever = new Alert(ids, user, flags);
  return alertRetriever.get();
};

export const getFormattedObject = async (id: number, user: number) => {
  const alertRetriever = new Alert([id], user, []);
  const alerts = await alertRetriever.get();
  return alerts[0];
};
