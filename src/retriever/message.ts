import AbstractRetriever from './abstractRetriever';
import knex from '../services/knex';
import User from './user';

export default class Message extends AbstractRetriever {
  protected cacheLifetime = 86400;

  private cachePrefix: string = 'message';

  private static async getMessages(ids: Array<number>) {
    const results = await knex
      .from('Messages as m')
      .select(
        'm.id as messageId',
        'm.conversation_id as messageConversationId',
        'm.user_id as messageUserId',
        'm.content as messageContent',
        'm.read_at as messageReadAt',
        'm.created_at as messageCreatedAt',
        'm.updated_at as messageUpdatedAt'
      )
      .whereIn('m.id', ids);

    return results.reduce((list, message) => {
      list[message.messageId] = message;
      return list;
    }, {});
  }

  private static format(data): Object {
    return {
      id: data.messageId,
      conversationId: data.messageConversationId,
      user: { id: data.messageUserId },
      content: data.messageContent,
      readAt: data.messageReadAt,
      createdAt: data.messageCreatedAt,
      updatedAt: data.messageUpdatedAt,
    };
  }

  private static async getUsers(messages: Array<any>) {
    const userIds = messages.map((message) => message.user.id);

    const userRetriever = new User(userIds, []);
    const rawUsers = await userRetriever.get();
    return userIds.reduce((list, userId, index) => {
      list[userId] = rawUsers[index];
      return list;
    }, {});
  }

  async get() {
    // grab canonical data
    const cachedMessages = await this.cacheGet(this.cachePrefix, this.ids);
    const uncachedIds = this.filterNullIndices(cachedMessages);
    const uncachedMessages = await Message.getMessages(uncachedIds);
    const messages = this.ids.map((id, index) => {
      if (cachedMessages[index] !== null) return cachedMessages[index];
      return Message.format(uncachedMessages[id]);
    });

    // write formatted data back to the cache
    if (uncachedIds.length > 0) {
      messages.map(async (message) => {
        await this.cacheSet(this.cachePrefix, message.id, message);
      });
    }

    // grab data from related caches
    const users = await Message.getUsers(messages);

    // merge related data in
    return messages.map((message) => {
      message.user = users[message.user.id] || null;
      return message;
    });
  }

  async invalidate() {
    this.ids.map(async (id) => {
      await this.cacheDrop(this.cachePrefix, id);
    });
  }
}

export const invalidateObjects = async (ids: Array<number>) => {
  const messageRetriever = new Message(ids, []);
  await messageRetriever.invalidate();
};

export const invalidateObject = async (id: number) => {
  const messageRetriever = new Message([id], []);
  await messageRetriever.invalidate();
};

export const getFormattedObjects = async (ids: Array<number>) => {
  const messageRetriever = new Message(ids, []);
  return messageRetriever.get();
};

export const getFormattedObject = async (id: number) => {
  const messageRetriever = new Message([id], []);
  const messages = await messageRetriever.get();
  return messages[0];
};
