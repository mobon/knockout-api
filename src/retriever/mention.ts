import AbstractRetriever from './abstractRetriever';
import knex from '../services/knex';
import { getFormattedObjects as getUserObjects } from './user';

export default class Mention extends AbstractRetriever {
  protected cacheLifetime = 604800;

  private cachePrefix: string = 'mention';

  private static async getMentions(ids: Array<number>) {
    const results = await knex('Mentions')
      .select(
        'Mentions.id as mentionId',
        'Mentions.post_id as postId',
        'Mentions.content',
        'Mentions.created_at as createdAt',
        'Mentions.thread_id as threadId',
        'Mentions.page as threadPage',
        'Mentions.thread_title as threadTitle',
        'Mentions.mentioned_by as mentionedBy'
      )
      .whereIn('Mentions.id', ids);

    return results.reduce((map, mention) => {
      map[mention.mentionId] = mention;
      return map;
    }, {});
  }

  private static async getUsers(mentions) {
    const userIds: number[] = Array.from(
      new Set(mentions.map((result) => Number(result.mentionedBy)))
    );

    const users = await getUserObjects(userIds);
    return users.reduce((map, user) => {
      map[user.id] = user;
      return map;
    }, {});
  }

  async get() {
    // grab canonical data
    const cachedMentions = await this.cacheGet(this.cachePrefix, this.ids);
    const uncachedIds = this.filterNullIndices(cachedMentions);
    const uncachedMentions = await Mention.getMentions(uncachedIds);
    const mentions = this.ids.map((id, index) => {
      if (cachedMentions[index] !== null) return cachedMentions[index];
      if (typeof uncachedMentions[id] !== 'undefined') return uncachedMentions[id];
      return {};
    });

    // write formatted data back to the cache
    if (uncachedIds.length > 0) {
      mentions.map(async (mention) => {
        await this.cacheSet(this.cachePrefix, mention.id, mention);
      });
    }

    const users = await Mention.getUsers(mentions);

    return mentions.map((mention) => ({ ...mention, mentionedBy: users[mention.mentionedBy] }));
  }

  async invalidate() {
    this.ids.map(async (id) => {
      await this.cacheDrop(this.cachePrefix, id);
    });
  }
}

// legacy signitures, remove at soonest convenience
export const invalidateObjects = async (ids: Array<number>) => {
  const mentionRetriever = new Mention(ids, []);
  await mentionRetriever.invalidate();
};

export const invalidateObject = async (id: number) => {
  const mentionRetriever = new Mention([id], []);
  await mentionRetriever.invalidate();
};

export const getFormattedObjects = async (ids: Array<number>) => {
  const mentionRetriever = new Mention(ids, []);
  return mentionRetriever.get();
};

export const getFormattedObject = async (id: number) => {
  const mentionRetriever = new Mention([id], []);
  const mentions = await mentionRetriever.get();
  return mentions[0];
};
