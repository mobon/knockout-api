import AbstractRetriever from './abstractRetriever';
import knex from '../services/knex';
import RuleSchema from '../v2/schemas/rules/rule';

export default class Rule extends AbstractRetriever {
  protected cacheLifetime = 86400;

  private cachePrefix: string = 'rule';

  private static async getRules(ids: Array<number>) {
    const results = await knex
      .from('Rules')
      .select(
        'id as ruleId',
        'rulable_type as ruleRulableType',
        'rulable_id as ruleRulableId',
        'category as ruleCategory',
        'title as ruleTitle',
        'cardinality as ruleCardinality',
        'description as ruleDescription',
        'created_at as ruleCreatedAt',
        'updated_at as ruleUpdatedAt'
      )
      .whereIn('id', ids);

    return results.reduce((list, rule) => {
      list[rule.ruleId] = rule;
      return list;
    }, {});
  }

  private static format(data): RuleSchema {
    return {
      id: data.ruleId,
      rulableId: data.ruleRulableId,
      rulableType: data.ruleRulableType,
      category: data.ruleCategory,
      title: data.ruleTitle,
      cardinality: data.ruleCardinality,
      description: data.ruleDescription,
      createdAt: data.ruleCreatedAt,
      updatedAt: data.ruleUpdatedAt,
    };
  }

  async get() {
    // grab canonical data
    const cachedRules = await this.cacheGet(this.cachePrefix, this.ids);
    const uncachedIds = this.filterNullIndices(cachedRules);
    const uncachedRules = await Rule.getRules(uncachedIds);
    const rules = this.ids
      .filter((id, index) => cachedRules[index] !== null || uncachedRules[id])
      .map((id, index) => {
        if (cachedRules[index] !== null) return cachedRules[index];
        return Rule.format(uncachedRules[id]);
      });

    // write formatted data back to the cache
    if (uncachedIds.length > 0) {
      await Promise.all(rules.map((rule) => this.cacheSet(this.cachePrefix, rule.id, rule)));
    }

    return rules;
  }

  async invalidate() {
    this.ids.map(async (id) => {
      await this.cacheDrop(this.cachePrefix, id);
    });
  }
}

export const invalidateObjects = async (ids: Array<number>) => {
  const ruleRetriever = new Rule(ids, []);
  await ruleRetriever.invalidate();
};

export const invalidateObject = async (id: number) => {
  const ruleRetriever = new Rule([id], []);
  await ruleRetriever.invalidate();
};

export const getFormattedObjects = async (ids: Array<number>, flags = []) => {
  const ruleRetriever = new Rule(ids, flags);
  return ruleRetriever.get();
};

export const getFormattedObject = async (id: number) => {
  const ruleRetriever = new Rule([id], []);
  const rules = await ruleRetriever.get();
  return rules[0];
};
