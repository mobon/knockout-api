const onDuplicateUpdate = (knex, tableName, data) => {
  const firstData = data[0] ? data[0] : data;

  return knex
    .raw(
      knex(tableName)
        .insert(data)
        .toQuery() +
        ' ON DUPLICATE KEY UPDATE ' +
        Object.getOwnPropertyNames(firstData)
          .map(field => {
            return `${field}=VALUES(${field})`;
          })
          .join(', ')
    )
    .then(rows => rows);
};

export default onDuplicateUpdate;
