const isProxy = (ip) => {

	const ip2proxy = require("ip2proxy-nodejs");
	const Netmask = require("netmask").Netmask;

	const cidr = {
		cloudflare: {
			ipv4: [
				'173.245.48.0/20',
				'103.21.244.0/22',
				'103.22.200.0/22',
				'103.31.4.0/22',
				'141.101.64.0/18',
				'108.162.192.0/18',
				'190.93.240.0/20',
				'188.114.96.0/20',
				'197.234.240.0/22',
				'198.41.128.0/17',
				'162.158.0.0/15',
				'104.16.0.0/12',
				'172.64.0.0/13',
				'131.0.72.0/22'
			],
			ipv6: [
				'2400:cb00::/32',
				'2606:4700::/32',
				'2803:f800::/32',
				'2405:b500::/32',
				'2405:8100::/32',
				'2a06:98c0::/29',
				'2c0f:f248::/32'
			]
		}
	}

	for (let i=0; i<cidr.cloudflare.ipv4.length; i++) {
		try {
			const block = new Netmask(cidr.cloudflare.ipv4[i]);
			if (block.contains(ip)) {
				return true;
			}
		} catch (e) {
			break;
		}
	}

	for (let i=0; i<cidr.cloudflare.ipv6.length; i++) {
		try {
			const block = new Netmask(cidr.cloudflare.ipv6[i]);
			if (block.contains(ip)) {
				return true;
			}
		} catch (e) {
			break;
		}
	}

	const path = '/opt/IP2PROXY-LITE-PX6.BIN';
	let result = false;
	if (ip2proxy.Open(path) == 0) {
		result = ip2proxy.isProxy(ip) || ip2proxy.getProxyType(ip) == 'DCH';
	}
	ip2proxy.Close();
	return result;

};

export { isProxy };