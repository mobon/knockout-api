import sharp from 'sharp';

export const stripExifTags = (buffer: Buffer) => sharp(buffer).toBuffer();

export const imageMetadata = (buffer: Buffer) => sharp(buffer).metadata();

export const avatarGifToWebp = (buffer: Buffer) =>
  sharp(buffer, { pages: -1 }).toFormat('webp').toBuffer();

export const avatarToWebp = (buffer: Buffer) =>
  sharp(buffer)
    .resize(115, 115, {
      withoutEnlargement: true,
      fit: 'inside',
    })
    .toFormat('webp')
    .toBuffer();

export const backgroundToWebp = (buffer: Buffer) =>
  sharp(buffer)
    .resize(230, 460)
    .webp({ quality: 75, nearLossless: true, lossless: true })
    .toFormat('webp')
    .toBuffer();
