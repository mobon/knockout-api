import knex from '../services/knex';

import Rule, { getFormattedObject, getFormattedObjects, invalidateObject } from '../retriever/rule';
import CreateRuleRequest from '../v2/schemas/rules/createRuleRequest';

export const storeRule = async ({
  rulable_type = null,
  rulable_id = null,
  category,
  title,
  cardinality,
  description,
}: CreateRuleRequest): Promise<Rule> => {
  const rule = await knex('Rules')
    .insert({
      rulable_type: rulable_type || null,
      rulable_id: rulable_id || null,
      category: category.trim(),
      title: title.trim(),
      cardinality: Number(cardinality) || 1,
      description,
    })
    .then((results) => results[0]);

  return getFormattedObject(rule);
};

export const updateRule = async ({
  ruleId,
  category,
  title,
  cardinality,
  description,
}: {
  ruleId: number;
  category?: string;
  title?: string;
  cardinality?: number;
  description?: string;
}) => {
  await knex('Rules').where('id', ruleId).update({ category, title, cardinality, description });

  await invalidateObject(ruleId);

  return getFormattedObject(ruleId);
};

export const indexRules = async ({
  rulableType = null,
  rulableId = null,
}: {
  rulableType?: string;
  rulableId?: number;
}) => {
  const query = await knex('Rules')
    .select('id')
    .where('rulable_type', rulableType || null)
    .andWhere('rulable_id', rulableId || null);

  return getFormattedObjects(query.map((item) => item.id));
};
