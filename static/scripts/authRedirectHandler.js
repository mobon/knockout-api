// @ts-check
// Please keep in mind this file is not transpiled by Babel
// Use ES5-compatible JS here!

/**
 * @type {Window}
 */
var parent = window.opener;

try {
  var params = new URLSearchParams(location.search);
  var base64User = params.get('user');
  var userJson = decodeURIComponent(base64User);
  var user = JSON.parse(userJson);
  parent.postMessage({ isLoginCallback: true, user: user }, '*');
}
catch (err) {
  parent.postMessage({ isLoginCallback: true, error: err.message }, '*');
}
finally {
  window.close();
}
